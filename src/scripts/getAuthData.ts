import { NextApiRequest, NextApiResponse } from 'next';

export interface AuthProps {
    req: NextApiRequest;
    res: NextApiResponse;
}

export interface CommonComponentDataProps {
    accessToken: string;
    hasRefreshToken?: boolean;
    expiresAt?: string;
}

const getAuthData = async ({ req }: AuthProps) => {
    const accessToken = req.cookies.access_token || '';
    const refreshToken = req.cookies.refresh_token || '';
    const expiresAt = req.cookies.expires_at || '';

    const commonData: CommonComponentDataProps = {
        accessToken,
        hasRefreshToken: Boolean(refreshToken),
        expiresAt,
    };

    return {
        ...commonData,
    };
};

export default getAuthData;
