import { useRouter } from 'next/router';
import { useEffect } from 'react';

export const useRedirectToNotEmptyPage = ({
    activePage,
    itemsPerPageCount,
    total: unsafeTotal,
    shallow = true,
    pageKey = 'page',
}: {
    activePage: number;
    itemsPerPageCount: number;
    total?: number;
    shallow?: boolean;
    pageKey?: string;
}) => {
    const { query, push } = useRouter();
    const total = unsafeTotal || 0;

    useEffect(() => {
        if (total === 0) return;

        const currentOffset = activePage * itemsPerPageCount;

        const newPage = Math.ceil(total / itemsPerPageCount);

        if (currentOffset - total >= itemsPerPageCount) {
            push({ query: { ...query, [pageKey]: newPage } }, undefined, {
                shallow,
            });
        }
    }, [activePage, itemsPerPageCount, total, push, query, pageKey, shallow]);
};
