import { useCallback, useEffect, useState } from 'react';

interface RowData {
    id: number;
}

type Payload<T extends RowData> = {
    [K in keyof T]: {
        id: number;
        column: K;
        value: T[K];
    };
};

export const useEditableTableRows = <T extends RowData>(
    getInitialData: (id: number) => T,
    reduce: (old: T, payload: Payload<T>[keyof T]) => T
) => {
    const [extraRowsData, setExtraRowsData] = useState<Record<number, T>>({});

    useEffect(() => () => setExtraRowsData({}), []);

    const onRowChange = useCallback(
        <TCol extends keyof T>(p: Payload<T>[TCol]) => {
            if (!p.id) return;

            setExtraRowsData(old => {
                const newOld = { ...old };

                if (!(p.id in newOld)) newOld[p.id] = getInitialData(p.id);

                newOld[p.id] = reduce(newOld[p.id], p as never as Payload<T>[TCol]);

                return newOld;
            });
        },
        [getInitialData, reduce]
    );

    return { extraRowsData, setExtraRowsData, onRowChange };
};
