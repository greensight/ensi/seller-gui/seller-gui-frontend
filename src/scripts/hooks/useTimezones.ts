import timezones from 'countries-and-timezones';

const list = Object.values(timezones.getAllTimezones());
// создает список временных зон из библиотеки countries-and-timezones,
// сортирует их по смещению UTC и создает объекты с метками и значениями для использования.
const options = list
    .sort((a, b) => b.utcOffset - a.utcOffset)
    .map(({ name, utcOffsetStr }) => ({
        value: name,
        label: `${name} ${utcOffsetStr}`,
    }));

export const useTimezones = () => options;
