import { useMemo } from 'react';

import { useCurrentUser } from '@api/auth';

enum BaseActionEnum {
    view = 'view',
    edit = 'edit',
    delete = 'delete',
    create = 'create',
}

type BaseAction = keyof typeof BaseActionEnum;
type Action = 'all' | BaseAction;
type CommonSections = 'ID';

type AccessUnit<T, TActions extends string = Action> = Record<TActions, T>;

type AccessSection<T, TActions extends string = Action> = {
    [key: string]: AccessSection<T> | Partial<AccessUnit<T, TActions>>;
};

type ListActions = 'view' | 'edit' | 'delete' | 'create';

export type AccessMatrix = Record<CommonSections, AccessSection<number> | Partial<AccessUnit<number>>> & {
    LIST: AccessSection<number> | Partial<AccessUnit<number, ListActions>>;
};

export type ReturnedMatrix<T> = {
    [K in keyof T]: T[K] extends object ? ReturnedMatrix<T[K]> : boolean;
};

const parseMatrix = <T extends Record<string, any>>(matrix: T, accesses: number[]) => {
    const keys = Object.keys(matrix);
    return keys.reduce(
        (parsedMatrix, key: string) => {
            if (parsedMatrix[key] !== undefined) return parsedMatrix;

            const value = matrix[key];
            if (key === 'all' && typeof value === 'number') {
                const access = accesses.includes(value);
                Object.keys(BaseActionEnum).forEach(k => {
                    parsedMatrix[k] = access;
                });
                return parsedMatrix;
            }

            if (typeof value === 'number') {
                parsedMatrix[key] = accesses.includes(value);
                return parsedMatrix;
            }

            if (typeof value === 'object') {
                parsedMatrix[key] = parseMatrix(value, accesses);
            }

            return parsedMatrix;
        },
        {} as Record<string, any>
    );
};

export const useAccess = <T extends AccessMatrix>(accessMatrix: T) => {
    const { data: userData } = useCurrentUser();

    return useMemo(
        () => parseMatrix(accessMatrix, userData?.data.rights_access || []),
        [accessMatrix, userData]
    ) as ReturnedMatrix<T>;
};
