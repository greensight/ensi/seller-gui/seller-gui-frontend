const validateInn = (value?: string) => {
    if (!value) return true;

    const inn = value;

    let isCorrect = false;
    let checkDigit;
    let firstCheckDigit;
    let secondCheckDigit;

    const countCheckDigit = (str: string, coefficients: number[]) => {
        const checksum = coefficients.reduce((sum, coefficient, index) => sum + coefficient * Number(str[index]), 0);

        return (checksum % 11) % 10;
    };

    switch (inn.length) {
        case 10:
            checkDigit = countCheckDigit(inn, [2, 4, 10, 3, 5, 9, 4, 6, 8]);
            if (checkDigit === parseInt(inn[9], 10)) {
                isCorrect = true;
            }
            break;
        case 12:
            firstCheckDigit = countCheckDigit(inn, [7, 2, 4, 10, 3, 5, 9, 4, 6, 8]);
            secondCheckDigit = countCheckDigit(inn, [3, 7, 2, 4, 10, 3, 5, 9, 4, 6, 8]);
            if (firstCheckDigit === parseInt(inn[10], 10) && secondCheckDigit === parseInt(inn[11], 10)) {
                isCorrect = true;
            }
            break;
        default:
    }

    return isCorrect;
};

export default validateInn;
