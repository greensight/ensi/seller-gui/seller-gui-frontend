import { MenuItemProps } from '@components/Sidebar/types';

import menuIcons from '@scripts/menuIcons';

const menu: MenuItemProps[] = [
    {
        text: 'Настройки',
        Icon: menuIcons.settings,
        code: 'settings',
        subMenu: [
            { text: 'Основные данные', link: '/settings/main-info', code: 'settings_main_data' },
            {
                text: 'Пользователи',
                link: '/settings/seller-users',
                Icon: menuIcons.users,
                code: 'settings_users',
            },
            { text: 'Склады', link: '/settings/stores', Icon: menuIcons.package, code: 'settings_stores' },
        ],
    },
];

const enrichMenu = (menuItems: MenuItemProps[], key?: string) => {
    const enrichedMenu = menuItems.slice();
    enrichedMenu.forEach(item => {
        const id = `${key}${item.code}`;
        item.id = id;
        if (item.subMenu) enrichMenu(item.subMenu, id);
    });
    return enrichedMenu;
};

interface FlatMenuItem {
    link?: string;
    text: string;
}

export interface FlatMenuItemExtended extends FlatMenuItem {
    parent: FlatMenuItem[];
}

/** нужно для формирования хлебных крошек */
const flatMenu = (menuItems: MenuItemProps[], parent: FlatMenuItem[] = []) =>
    menuItems.reduce((acc, val) => {
        if (val.link) acc.push({ text: val.text, link: val?.link, parent });
        if (val.subMenu) acc.push(...flatMenu(val.subMenu, [...parent, { text: val.text, link: val.link }]));
        return acc;
    }, [] as FlatMenuItemExtended[]);

export const preparedFlatMenu = flatMenu(menu);

export default enrichMenu(menu);
