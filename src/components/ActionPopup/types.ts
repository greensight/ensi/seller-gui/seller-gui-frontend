import { ReactNode } from 'react';

import { ActionEnum, ThemesEnum } from './enums';

export type ActionState = {
    onAction: () => Promise<any> | void;
    title: string;
    popupAction: ActionEnum;
    popupTheme: ThemesEnum;
    open: boolean;
    children: ReactNode;
};
