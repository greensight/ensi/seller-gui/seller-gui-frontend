import { KeyboardEvent, MouseEvent, ReactNode, useMemo } from 'react';

import Popup, { PopupProps } from '@controls/future/Popup';
import { Content } from '@controls/future/Popup/components/Content';

import { Button, colors, scale, typography } from '@scripts/gds';

import SuccessIcon from '@icons/20/checkCircle.svg';
import ErrorIcon from '@icons/20/closedCircle.svg';
import DeleteIcon from '@icons/20/delete.svg';
import InfoIcon from '@icons/20/info.svg';
import WarningIcon from '@icons/20/warning.svg';

import { ActionEnum, ThemesEnum } from './enums';

export * from './enums';
export * from './types';

export interface IActionPopupProps extends Omit<PopupProps, 'title' | 'onBackdropClick'> {
    action?: ActionEnum;
    title?: string;
    leftAddonIconTheme?: `${ThemesEnum}`;
    onAction?: () => void;
    disableAction?: boolean;
    disableClose?: boolean;
    disableFooter?: boolean;
    blockButtons?: boolean;
    onBackdropClick?: (
        event: MouseEvent<HTMLElement> | KeyboardEvent<HTMLElement>,
        reason?: 'backdropClick' | 'escapeKeyDown' | 'closerClick'
    ) => void;
}

const ActionPopup = ({
    action,
    title,
    leftAddonIconTheme,
    children,
    onClose,
    onAction,
    onBackdropClick,
    disableAction,
    disableClose,
    blockButtons = true,
    disableFooter,
    ...props
}: IActionPopupProps) => {
    const btnParams = useMemo(() => {
        switch (action) {
            case ActionEnum.DELETE: {
                return {
                    theme: 'dangerous',
                    actionButtonText: 'Удалить',
                    closeButtonText: 'Не удалять',
                };
            }
            case ActionEnum.COPY: {
                return {
                    theme: 'primary',
                    actionButtonText: 'Дублировать',
                    closeButtonText: 'Не дублировать',
                };
            }
            case ActionEnum.CONFIRM: {
                return {
                    theme: 'primary',
                    actionButtonText: 'Подтвердить',
                    closeButtonText: 'Отмена',
                };
            }
            case ActionEnum.UNTIE: {
                return {
                    theme: 'primary',
                    actionButtonText: 'Отвязать',
                    closeButtonText: 'Не отвязывать',
                };
            }
            default: {
                return {
                    theme: 'primary',
                    actionButtonText: 'Сохранить',
                    closeButtonText: 'Не сохранять',
                };
            }
        }
    }, [action]);

    const leftAddonIcon = (): ReactNode => {
        switch (leftAddonIconTheme) {
            case ThemesEnum.WARNING: {
                return <WarningIcon css={{ fill: colors?.warning }} />;
            }
            case ThemesEnum.ERROR: {
                return <ErrorIcon css={{ fill: colors?.danger }} />;
            }
            case ThemesEnum.SUCCESS: {
                return <SuccessIcon css={{ fill: colors?.success }} />;
            }
            case ThemesEnum.DELETE: {
                return <DeleteIcon css={{ fill: colors?.danger }} />;
            }
            case ThemesEnum.INFO: {
                return <InfoIcon css={{ fill: colors?.primary }} />;
            }
            default: {
                return null;
            }
        }
    };

    return (
        <Popup hasCloser={false} onClose={onBackdropClick ?? onClose} {...props}>
            <Popup.Header
                leftAddons={leftAddonIcon()}
                addonCSS={{
                    paddingRight: scale(2),
                    height: 'auto',
                    minHeight: 'auto',
                }}
            >
                <p css={typography('h3')}>{title}</p>
            </Popup.Header>
            {children}
            {!disableFooter && (
                <Popup.Footer css={{ display: 'flex', justifyContent: 'flex-end' }}>
                    <Button onClick={onClose} theme="secondary" disabled={disableClose} block={blockButtons}>
                        {btnParams.closeButtonText}
                    </Button>
                    {onAction && (
                        <Button
                            onClick={() => onAction()}
                            theme={btnParams.theme}
                            disabled={disableAction}
                            block={blockButtons}
                        >
                            {btnParams.actionButtonText}
                        </Button>
                    )}
                </Popup.Footer>
            )}
        </Popup>
    );
};

ActionPopup.Content = Content;

export default ActionPopup;
