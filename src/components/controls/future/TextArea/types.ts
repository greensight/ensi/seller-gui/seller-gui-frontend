import { CSSObject } from '@emotion/react';
import { FieldInputProps, FieldMetaProps } from 'formik';
import { HTMLProps, ReactNode, Ref } from 'react';

import { FormControlProps } from '../FormControl';

export type TextareaProps = Omit<
    HTMLProps<HTMLDivElement>,
    'ref' | 'size' | 'value'
> & {
    /** Input name (inner) */
    name?: string;
    /**
     * Значение поля ввода
     */
    value?: string;
    /** Formik field object (inner) */
    field?: FieldInputProps<string>;
    /** Formik meta object (inner) */
    meta?: FieldMetaProps<any>;
    /** Minimum number of visible rows */
    minRows?: number;
    /** Maximum number of visible rows */
    maxRows?: number;
    /** Maximum length of value */
    maxLength?: number;
    /** Threshold in percentage of limit */
    threshold?: number;

    block?: boolean;

    /** resize flag */
    isResize?: boolean;
    /**
     * Лейбл компонента
     */
    label?: ReactNode;
    /**
     * Слот под инпутом
     */
    bottomAddons?: ReactNode;

    labelWrap?: FormControlProps['labelWrap'];
    /**
     * Отображение ошибки
     */
    error?: ReactNode | boolean;
    /**
     * Текст подсказки
     */
    hint?: ReactNode;
    /**
     * Дополнительный класс
     */
    className?: string;
    /**
     * Дополнительный стиль поля ввода
     */
    textAreaCSS?: CSSObject;
    /**
     * Дополнительный стиль для лейбла
     */
    labelCSS?: CSSObject;
    /**
     * Дополнительный стиль для аддонов
     */
    leftAddonsCSS?: CSSObject;
    rightAddonsCSS?: CSSObject;
    /**
     * Слот слева
     */
    leftAddons?: ReactNode;
    /**
     * Слот справа
     */
    rightAddons?: ReactNode;
    /**
     * Ref для обертки TextArea
     */
    wrapperRef?: Ref<HTMLDivElement>;
    /**
     * Флаг отображения ошибки
     */
    showError?: boolean;
    /**
     * Дополнительный стиль для поля
     */
    fieldCSS?: CSSObject;
    /**
     * Дополнительный стиль для обертки
     */
    wrapperCSS?: CSSObject;
};
