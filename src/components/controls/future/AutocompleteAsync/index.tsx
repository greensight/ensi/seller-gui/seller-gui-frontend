import { forwardRef, useCallback, useEffect, useMemo, useRef, useState } from 'react';
import deepEqual from 'react-fast-compare';

import { useLazyLoading } from '@controls/future/Select/presets/useLazyLoading';

import { Button, scale } from '@scripts/gds';
import { usePrevious } from '@scripts/hooks';

import PreloaderIcon from '@icons/preloader.svg';
import CloseIcon from '@icons/small/closed.svg';

import { Autocomplete } from '../Autocomplete';
import { OptionProps, OptionShape } from '../Select/types';
import { SimpleSelectWithTags } from '../SelectWithTags';
import { AutocompleteAsyncProps } from './types';
import { FieldProps } from '../Form';

const DEBOUNCE_TIMEOUT = 300;

type ValOrArr<T> = T | T[];

type Props = AutocompleteAsyncProps & FieldProps<
    ValOrArr<string | number | object | null>
>;

const AutocompleteAsync = forwardRef<HTMLInputElement, Props>(
    (
        {
            collapseTagList = false,
            multiple = false,
            clearOnSelect = true,
            meta,
            field,
            helpers,
            onOpen,
            onChange,
            onInput,
            asyncOptionsByValuesFn,
            asyncSearchFn,
            onOptionsChange,
            fieldProps = {},
            extractKeyFromValue = val => `${val}`,
            onClear,
            ...props
        },
        ref
    ) => {
        const [valuesMap, setValuesMap] = useState(new Map<any, OptionShape>());

        const selectedValues = useMemo(() => {
            if (Array.isArray(field?.value)) return field?.value.filter(v => v !== '') as any[];

            return field?.value !== null && field?.value !== undefined && field?.value !== '' ? [field?.value] : [];
        }, [field?.value]);

        const prevSelectedValues = usePrevious(selectedValues);

        const [isFetchingValues, setFetchingValues] = useState(false);

        const {
            setValue,
            reset,
            isLoading,
            isNotFound,
            optionsProps: { optionsListProps, ...lazyProps },
        } = useLazyLoading({
            onOptionsChange,
            optionsFetcher: async (queryString, offset, limit) => {
                if (!asyncSearchFn) {
                    return {
                        options: [],
                        hasMore: false,
                    };
                }
                try {
                    const res = await asyncSearchFn(queryString, offset, queryString.length > 0 ? limit : -1);
                    return res;
                } catch {
                    return {
                        options: [],
                        hasMore: false,
                    };
                }
            },
            initialOffset: 0,
            limit: 15,
            isValuesLoading: isFetchingValues,
            clearOnClose: multiple,
        });

        const abortFetchingSelectedOptionsRef = useRef<() => void>();
        const selectedOptionsFetchingAbortControllerRef = useRef<AbortController>();

        const fetchOptionsByValuesRef = useRef<typeof asyncOptionsByValuesFn>();
        fetchOptionsByValuesRef.current = asyncOptionsByValuesFn;

        const valuesMapRef = useRef(valuesMap);
        valuesMapRef.current = valuesMap;

        const fetchUnknownValues = useCallback(() => {
            const unknownValues = selectedValues.filter(e => !valuesMapRef.current.has(e));

            if (!unknownValues.length) return;

            setFetchingValues(true);
            new Promise<OptionShape[]>((resolve, reject) => {
                selectedOptionsFetchingAbortControllerRef.current?.abort();
                const abortController = new AbortController();
                selectedOptionsFetchingAbortControllerRef.current = abortController;
                abortFetchingSelectedOptionsRef.current?.();
                abortFetchingSelectedOptionsRef.current = reject;
                fetchOptionsByValuesRef
                    .current?.(unknownValues, abortController)
                    .then(res => {
                        resolve(res);
                    })
                    .catch(() => {});
            })
                .then(res => {
                    abortFetchingSelectedOptionsRef.current = undefined;

                    res.forEach(e => {
                        valuesMapRef.current.set(e.value, e);
                    });

                    setValuesMap(new Map(valuesMapRef.current));

                    setFetchingValues(false);
                })
                .catch(() => {});
        }, [selectedValues]);

        const fetchRef = useRef<() => void>();

        useEffect(() => {
            fetchRef.current = fetchUnknownValues;
        }, [fetchUnknownValues]);

        const fetchTimerRef = useRef<ReturnType<typeof setTimeout>>();

        useEffect(() => {
            abortFetchingSelectedOptionsRef.current?.();

            /* Дебаунсим изменения, чтобы не отправлять запрос на каждый чих */
            if (fetchTimerRef.current) {
                clearTimeout(fetchTimerRef.current);
            }

            fetchTimerRef.current = setTimeout(() => {
                /*
                 * После дебаунса необходимо вызвать функцию-загрузчик
                 */
                fetchRef.current?.();
            }, DEBOUNCE_TIMEOUT);

            fetchUnknownValues();
        }, [fetchUnknownValues]);

        const selectedOptions = useMemo(() => {
            const valuesWithKnownKey = selectedValues.filter(e => valuesMap.has(e));
            const valuesWithUnKnownKey = selectedValues.filter(e => !valuesMap.has(e));
            const knownTags = valuesWithKnownKey.map<OptionShape>(e => ({
                key: valuesMap.get(e)!.key,
                disabled: valuesMap.get(e)!.disabled,
                value: e,
            }));

            const loadingTags = valuesWithUnKnownKey.map<OptionShape>(e => ({
                key: extractKeyFromValue(e),
                isPreloader: true,
                value: e,
            }));

            return [...knownTags, ...loadingTags];
        }, [selectedValues, valuesMap, extractKeyFromValue]);

        const [isSingleValueSet, setSingleValueSet] = useState(false);

        const prevFieldValue = usePrevious(field?.value);
        useEffect(() => {
            if (
                field?.value !== undefined &&
                !deepEqual(prevFieldValue, field?.value) &&
                selectedOptions[0]?.key !== optionsListProps.inputProps.value
            ) {
                setSingleValueSet(false);
            }
        }, [field?.value, optionsListProps.inputProps.value, prevFieldValue, props.name, selectedOptions]);

        useEffect(() => {
            if (isSingleValueSet) return;
            if (multiple) return;
            if (!selectedValues.length) return;

            if (selectedOptions.length && !selectedOptions[0].isPreloader) {
                setValue(selectedOptions[0].key, false);
                setSingleValueSet(true);
            }
        }, [isSingleValueSet, multiple, selectedOptions, selectedValues.length, setValue]);

        useEffect(() => {
            if (!selectedValues.length && prevSelectedValues?.length) {
                setValue('');
            }
        }, [selectedValues.length, prevSelectedValues?.length, setValue]);

        const wasSettingValueRef = useRef(false);

        if (!multiple) {
            return (
                <Autocomplete
                    ref={ref}
                    {...props}
                    onInput={e => {
                        optionsListProps.inputProps.onChange(e, { value: e.currentTarget.value });
                        onInput?.(e);

                        if (!e.currentTarget.value) {
                            reset();

                            if (typeof field?.onChange === 'function') {
                                field.onChange({
                                    target: {
                                        value: null,
                                    },
                                });

                                return;
                            }

                            if (helpers) {
                                helpers.setValue(null);
                            }
                        }
                    }}
                    selected={selectedOptions}
                    onChange={payload => {
                        wasSettingValueRef.current = true;
                        onChange?.(payload);

                        setValue(payload.selectedMultiple[0].key, false);

                        payload.selectedMultiple.forEach(e => {
                            if (typeof e === 'string') return;
                            valuesMapRef.current.set(e.value, e);
                        });

                        setValuesMap(new Map(valuesMapRef.current));


                        const newValue = payload.selectedMultiple.map(e =>
                            typeof e === 'string' ? e : e.value
                        )[0];

                        if (typeof field?.onChange === 'function') {
                            field.onChange({
                                target: {
                                    value: newValue,
                                },
                            });

                            return;
                        }

                        helpers?.setValue(newValue);
                    }}
                    error={meta?.error}
                    fieldProps={{
                        ...fieldProps,
                        inputProps: {
                            ...fieldProps.inputProps,
                            ...(!isLoading && {
                                placeholder: props.placeholder,
                            }),
                        },
                        rightAddons: (
                            <>
                                {!!selectedValues.length && !isLoading && !props.hideClearButton && !props.disabled && (
                                    <button
                                        type="button"
                                        onClick={e => {
                                            e.stopPropagation();

                                            helpers?.setValue(null);
                                            reset();

                                            wasSettingValueRef.current = true;

                                            onClear?.();
                                        }}
                                        css={{
                                            display: 'flex',
                                            justifyContent: 'center',
                                            alignItems: 'center',
                                            ':hover': {
                                                opacity: 0.5,
                                            },
                                        }}
                                    >
                                        <CloseIcon />
                                    </button>
                                )}
                                {isLoading ? <PreloaderIcon css={{ width: scale(2) }} /> : null}
                            </>
                        ),
                    }}
                    optionsListProps={{
                        ...optionsListProps,
                        emptyPlaceholder: (
                            <div style={{ display: 'flex', justifyContent: 'center' }}>
                                {isNotFound ? (
                                    <div
                                        css={{
                                            display: 'flex',
                                            flexDirection: 'row',
                                            alignItems: 'center',
                                            gap: scale(1),
                                        }}
                                    >
                                        <p>Ничего не найдено</p>
                                        <Button
                                            onClick={() => {
                                                reset();
                                            }}
                                            theme="outline"
                                        >
                                            Сбросить
                                        </Button>
                                    </div>
                                ) : null}
                                {isLoading ? 'Поиск...' : null}
                                {!isLoading && !isNotFound ? 'Начинайте вводить' : null}
                            </div>
                        ),
                    }}
                    {...lazyProps}
                    showEmptyOptionsList
                    multiple={false}
                    onOpen={payload => {
                        onOpen?.(payload);
                        lazyProps.onOpen(payload);

                        if (!payload.open && selectedOptions.length) {
                            if (wasSettingValueRef.current) {
                                wasSettingValueRef.current = false;
                            } else {
                                setValue(selectedOptions[0].key);
                            }
                        }
                    }}
                    Option={(optionProps: OptionProps) =>
                        lazyProps.Option({
                            ...optionProps,
                            selected: selectedValues.includes(optionProps.option.value),
                        })
                    }
                    {...props}
                    value={optionsListProps.inputProps.value}
                />
            );
        }

        return (
            <SimpleSelectWithTags
                ref={ref}
                onInput={e => {
                    optionsListProps.inputProps.onChange(e, { value: e.target.value });
                    onInput?.(e);
                }}
                selected={selectedOptions}
                onChange={payload => {
                    onChange?.(payload);

                    payload.selectedMultiple.forEach(e => {
                        if (typeof e === 'string') return;
                        valuesMapRef.current.set(e.value, e);
                    });

                    setValuesMap(new Map(valuesMapRef.current));

                    if (clearOnSelect) reset();

                    if (!helpers) return;

                    helpers.setValue(payload.selectedMultiple.map(e => (typeof e === 'string' ? e : e.value)));
                }}
                collapseTagList={collapseTagList}
                resetOnChange={false}
                resetOnClose={false}
                error={meta?.error}
                fieldProps={{
                    ...fieldProps,
                    ...(isLoading && {
                        rightAddons: <PreloaderIcon css={{ width: scale(2) }} />,
                    }),
                }}
                optionsListProps={{
                    ...optionsListProps,
                    emptyPlaceholder: (
                        <div style={{ display: 'flex', justifyContent: 'center' }}>
                            {isNotFound ? (
                                <div>
                                    <p>Ничего не найдено</p>
                                    <Button
                                        onClick={() => {
                                            reset();
                                        }}
                                    >
                                        Сбросить
                                    </Button>
                                </div>
                            ) : null}
                            {isLoading ? 'Поиск...' : null}
                            {!isLoading && !isNotFound ? 'Начинайте вводить' : null}
                        </div>
                    ),
                }}
                isLoading={isFetchingValues}
                {...lazyProps}
                {...props}
                value={optionsListProps.inputProps.value}
            />
        );
    }
);

AutocompleteAsync.displayName = 'AutocompleteAsync';

export default AutocompleteAsync;
