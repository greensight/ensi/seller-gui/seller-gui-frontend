import type { useLazyLoadingProps } from '../Select/presets/useLazyLoading';
import type { OptionShape } from '../Select/types';
import type { SelectWithTagsProps } from '../SelectWithTags/types';

export interface AutocompleteAsyncProps
    extends Omit<SelectWithTagsProps, 'options' | 'selected' | 'value' | 'onInput' | 'onChange'> {
    asyncSearchFn: useLazyLoadingProps['optionsFetcher'];

    multiple?: boolean;

    /**
     * Фетчер-функция для получения опций из массива уже выбранных значений
     * @param values массив выбранных значений
     */
    asyncOptionsByValuesFn(values: any[], abortController: AbortController): Promise<OptionShape[]>;

    clearOnSelect?: boolean;

    onInput?: SelectWithTagsProps['onInput'];

    extractKeyFromValue?: (value: any) => string;

    /**
     * Обработчик выбора
     */
    onChange?: (payload: {
        selected?: OptionShape | null;
        selectedMultiple: Array<OptionShape | string>;
        initiator?: OptionShape | null;
        name?: string;
    }) => void;

    /**
     * Обработчик загрузки новых опций. Предполагается, 
     * что в родительском компоненте будет писаться в ref.current
     */
    onOptionsChange?: (options: OptionShape[]) => void;

    onClear?: () => void;
}
