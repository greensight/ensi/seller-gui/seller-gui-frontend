import { FieldHelperProps, FieldMetaProps } from 'formik';
import {
    ChangeEvent,
    FocusEvent,
    KeyboardEvent,
    MouseEvent,
    forwardRef,
    useCallback,
    useEffect,
    useRef,
    useState,
} from 'react';

import Calendar, { CalendarProps } from '@components/controls/Calendar';
import { dateInLimits } from '@components/controls/Calendar/utils';
import Tooltip, { TippyProps } from '@components/controls/Tooltip';
import Input from '@components/controls/future/Input';
import { InputProps } from '@components/controls/future/Input/types';

import { scale, useTheme } from '@scripts/gds';
import { useOnClickOutside } from '@scripts/hooks';

import CalendarIcon from '@icons/small/calendar.svg';

import Legend from '../Legend';
import {
    MAX_DATE_STRING_LENGTH,
    MAX_DATE_STRING_LENGTH_WITH_DOTS,
    formatDate,
    parseDateString,
    prepareInputValue,
} from './utils';

interface CalendarInputProps extends Omit<InputProps, 'onChange' | 'value'> {
    /** Параметры формика */
    field?: {
        value: number | null;
        onChange: (e: { target: { value: number | null }}) => void;
    };
    meta?: FieldMetaProps<number>;
    helpers?: FieldHelperProps<number | null>;

    /** Прятать ли календарик при выборе даты в календарике */
    hideCalendarOnChange?: boolean;

    calendarProps?: Omit<CalendarProps, 'minDate' | 'maxDate'>;

    /** Значение, контроллируемое извне */
    value?: number;
    /** Функция коллбэк, для контроля извне */
    onChange?: (date: number | null) => void;

    /** timestamp */
    minDate?: number;

    /** timestamp */
    maxDate?: number;

    /** Если  */
    onStateChange?: (changedValue: number) => void;

    /** Позиционирование типпи */
    placement?: TippyProps['placement'];
}

const CalendarInput = forwardRef<HTMLInputElement, CalendarInputProps>(
    (
        {
            field,
            helpers,
            hideCalendarOnChange = true,
            minDate = new Date('1950-01-01').getTime(),
            maxDate = new Date('2036-01-01').getTime(),
            calendarProps = {},
            disabled,
            value,
            onChange,
            placement = 'bottom-start',
            label,
            meta,
            name,
            isLegend = true,
            error,
            ...props
        },
        ref
    ) => {
        const { colors } = useTheme();

        const controlled = (helpers && field?.value !== undefined) || (value !== undefined && onChange);
        const controlledValue = field?.value ?? value;
        const initiatorRef = useRef<'calendar' | 'input' | 'outside'>('outside');
        const [open, setOpen] = useState(false);
        const containerRef = useRef<HTMLDivElement>(null);
        const calendarRef = useRef<HTMLDivElement>(null);

        const [stateValue, setStateValue] = useState<number>();
        const calendarValue = controlled ? controlledValue : stateValue;
        const [inputValue, setInputValue] = useState<string>();

        useEffect(() => {
            if (controlled) {
                const formattedValue = formatDate(controlledValue);

                /** если обновился инпут, то нужно обновить значение формы */
                if (initiatorRef.current === 'input') {
                    if (!inputValue?.length) {
                        onChange?.(null);

                        if (field?.onChange) {
                            field?.onChange({
                                target: {
                                    value: null,
                                }
                            });

                            return;
                        }
                        
                        helpers?.setValue(null);
                    }

                    if (inputValue?.length === MAX_DATE_STRING_LENGTH_WITH_DOTS) {
                        const parsedDate = parseDateString(inputValue);
                        helpers?.setValue(parsedDate);
                        if (onChange) onChange(parsedDate);
                    } 
                }

                /** Если значение controlledValue изменилось и не соответствует значению в инпуте */
                if (
                    (initiatorRef.current === 'outside' || initiatorRef.current === 'calendar') &&
                    formattedValue !== inputValue
                ) {
                    setInputValue(formatDate(controlledValue));
                }

                initiatorRef.current = 'outside';
            }
        }, [controlled, inputValue, helpers, controlledValue, onChange]);

        const handleKeyDown = useCallback(
            (event: KeyboardEvent<HTMLDivElement>) => {
                if ((event.target as HTMLElement).tagName === 'INPUT' && event.key === 'Enter') {
                    setOpen(!open);
                }

                if (event.key === 'Escape') {
                    setOpen(false);
                }
            },
            [open]
        );

        const handleClick = useCallback(() => {
            if (!open) setOpen(true);
        }, [open]);

        const handleBlur = useCallback(
            (e: FocusEvent<HTMLDivElement>) => {
                const target = (e.relatedTarget || document.activeElement) as HTMLDivElement;
                if (
                    calendarRef.current &&
                    calendarRef.current.contains(target) === false &&
                    !(containerRef.current && containerRef.current.contains(target))
                ) {
                    setOpen(false);
                }
            },
            [containerRef]
        );

        const handleInputKeyDown = useCallback((event: KeyboardEvent<HTMLInputElement>) => {
            if (['ArrowDown', 'ArrowUp'].includes(event.key) && calendarRef.current) {
                event.preventDefault();
                calendarRef.current.focus();
            }
        }, []);

        const handleCalendarChange = useCallback(
            (newDate: number) => {
                initiatorRef.current = 'calendar';
                const formattedDate = formatDate(newDate);
                setInputValue(formattedDate);
                if (hideCalendarOnChange) setOpen(false);
                if (controlled) {
                    helpers?.setValue(newDate);
                    if (onChange) onChange(newDate);
                } else {
                    setStateValue(newDate);
                }
            },
            [controlled, helpers, hideCalendarOnChange, onChange]
        );

        const handleInputChange = useCallback(
            (e: ChangeEvent<HTMLInputElement>) => {
                initiatorRef.current = 'input';
                const newInputValue = e.target.value.replace(/\D/g, '');

                if (newInputValue.length > MAX_DATE_STRING_LENGTH) return;
                const preparedNewInputValue = prepareInputValue(newInputValue);
                setInputValue(preparedNewInputValue);

                if (!controlled) {
                    setStateValue(parseDateString(preparedNewInputValue));
                }
            },
            [controlled]
        );

        const handleInputBlur = useCallback(
            (e: FocusEvent<HTMLInputElement>) => {
                const newDate = parseDateString(e.target.value);
                if (!dateInLimits(new Date(newDate), minDate, maxDate)) {
                    setInputValue('');
                    if (controlled) {
                        helpers?.setValue(NaN);
                        if (onChange) onChange(NaN);
                    } else {
                        setStateValue(NaN);
                    }
                }
                handleBlur(e);
            },
            [controlled, handleBlur, helpers, maxDate, minDate, onChange]
        );

        const handleCalendarWrapperMouseDown = useCallback((event: MouseEvent<HTMLDivElement>) => {
            /** Не дает инпуту терять фокус при выборе даты */
            event.preventDefault();
        }, []);

        useOnClickOutside(containerRef, () => setOpen(false));

        return (
            <>
                {isLegend && <Legend label={label} meta={meta} name={name} />}
                <div css={{ position: 'relative' }} onKeyDown={disabled ? undefined : handleKeyDown} ref={containerRef}>
                    <Tooltip
                        visible={open}
                        theme="none"
                        content={
                            <div onMouseDown={handleCalendarWrapperMouseDown} role="presentation">
                                <Calendar
                                    ref={calendarRef}
                                    value={calendarValue}
                                    onChange={handleCalendarChange}
                                    minDate={minDate}
                                    maxDate={maxDate}
                                    {...calendarProps}
                                />
                            </div>
                        }
                        placement={placement}
                    >
                        <Input
                            rightAddons={CalendarIcon && <CalendarIcon onClick={handleClick} />}
                            rightAddonsCSS={{ fill: colors?.grey800, paddingRight: scale(1) }}
                            disabled={disabled}
                            {...props}
                            meta={meta}
                            autoComplete="off"
                            value={inputValue}
                            onChange={disabled ? undefined : handleInputChange}
                            onKeyDown={disabled ? undefined : handleInputKeyDown}
                            onBlur={disabled ? undefined : handleInputBlur}
                            onFocus={disabled ? undefined : handleClick}
                            ref={ref}
                            isLegend={false}
                            label={undefined}
                            error={isLegend ? !!error : error}
                        />
                    </Tooltip>
                </div>
            </>
        );
    }
);

export default CalendarInput;
