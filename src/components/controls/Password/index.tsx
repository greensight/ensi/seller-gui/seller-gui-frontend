import { FieldHelperProps, FieldInputProps, FieldMetaProps } from 'formik';
import { ChangeEvent, HTMLProps, useState } from 'react';

import type { SVGRIcon } from '@customTypes/index';

import { scale, useTheme } from '@scripts/gds';
import { useFieldCSS } from '@scripts/hooks';

import EyeIcon from '@icons/small/eye.svg';
import EyeOffIcon from '@icons/small/eyeOff.svg';

export interface PasswordProps extends HTMLProps<HTMLInputElement> {
    /** Formik field object (inner) */
    field?: FieldInputProps<string>;
    /** Custom icon */
    Icon?: SVGRIcon;
    /** Formik helpers object (inner) */
    helpers?: FieldHelperProps<string>;
    /** Formik meta object (inner) */
    meta?: FieldMetaProps<string>;
}

const Password = ({ field, meta, autoComplete = 'off', ...props }: PasswordProps) => {
    delete props.helpers;
    delete props.Icon;
    const [isVisible, setIsVisible] = useState(false);
    const { colors } = useTheme();
    const { basicFieldCSS } = useFieldCSS(meta);

    const inputProps = { ...field, ...props };

    const handleChange = (event: ChangeEvent<HTMLInputElement>) => {
        const { value } = event.target;
        const sanitizedValue = value.replace(/\s/g, '');
        event.target.value = sanitizedValue;
        field?.onChange(event);
    };

    return (
        <div css={{ position: 'relative' }}>
            <input
                {...inputProps}
                autoComplete={autoComplete}
                type={isVisible ? 'text' : 'password'}
                onChange={handleChange}
                css={{ ...basicFieldCSS, ...(props.css as any), paddingRight: scale(6) }}
            />
            {autoComplete === 'off' && <input name={inputProps.name} type="password" style={{ display: 'none' }} />}
            <div
                css={{
                    position: 'absolute',
                    top: 0,
                    right: 0,
                    display: 'flex',
                    justifyContent: 'center',
                    alignItems: 'center',
                    width: scale(6),
                    height: '100%',
                }}
            >
                <button
                    type="button"
                    onClick={() => setIsVisible(!isVisible)}
                    css={{
                        width: '100%',
                        height: '100%',
                        color: isVisible ? colors?.black : colors?.grey200,
                        transition: 'fill ease 300ms',
                        ':focus': { outlineOffset: -2 },
                    }}
                >
                    {isVisible ? <EyeOffIcon title="Показать пароль" /> : <EyeIcon title="Скрыть пароль" />}
                </button>
            </div>
        </div>
    );
};

export default Password;
