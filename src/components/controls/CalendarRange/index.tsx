import { useFormikContext } from 'formik';
import { FC, useMemo, useRef } from 'react';
import { useFormContext } from 'react-hook-form';

import FutureForm from '@controls/future/Form';

import CalendarInput from '@components/controls/CalendarInput';
import Form from '@components/controls/Form';

import { Layout, scale } from '@scripts/gds';
import { getValueFromObject } from '@scripts/helpers';
import { useMedia } from '@scripts/hooks';

interface CalendarRangeProps {
    label?: string;
    nameFrom: string;
    nameTo: string;
    disabled?: boolean;
}

const useIsomorphicValues = ({ nameFrom, nameTo }: Pick<CalendarRangeProps, 'nameFrom' | 'nameTo'>) => {
    const oldConsoleWarning = console.warn;
    console.warn = () => {};
    const formikContext = useFormikContext();
    console.warn = oldConsoleWarning;
    const formContext = useFormContext();

    if (!formikContext && !formContext) {
        throw new Error('CalendarRange can only exist as a descendant of formik or react-hook-form');
    }

    if (formContext) {
        const [valueFrom, valueTo] = formContext.watch([nameFrom, nameTo]);
        return {
            values: {
                [nameFrom]: valueFrom,
                [nameTo]: valueTo,
            },
            form: 'rhf',
        };
    }

    return {
        values: formikContext.values as Record<string, any>,
        form: 'formik',
    };
};

const CalendarRange: FC<CalendarRangeProps> = ({ nameFrom, nameTo, label, disabled }) => {
    const { values, form } = useIsomorphicValues({ nameFrom, nameTo });
    const startDate = useMemo(() => (values ? +getValueFromObject(nameFrom, values, NaN) : NaN), [nameFrom, values]);
    const endDate = useMemo(() => (values ? +getValueFromObject(nameTo, values, NaN) : NaN), [nameTo, values]);
    const { md, xxl, xl, xs } = useMedia();

    const endRef = useRef<HTMLInputElement>(null);

    const FormField = form === 'rhf' ? FutureForm.Field : Form.Field;

    return (
        <Layout cols={{ xxxl: 2, xxs: 1 }}>
            <Layout.Item col={1}>
                <FormField name={nameFrom} disabled={disabled}>
                    <CalendarInput
                        label={label}
                        placeholder="От"
                        maxDate={endDate}
                        calendarProps={{
                            selectedFrom: startDate,
                            selectedTo: endDate,
                        }}
                        onChange={() => {
                            setTimeout(() => {
                                if (endRef?.current && !endDate) {
                                    endRef?.current?.click();
                                    endRef?.current?.focus();
                                }
                            }, 0);
                        }}
                    />
                </FormField>
            </Layout.Item>
            <Layout.Item
                col={1}
                align="end"
                css={{
                    position: 'relative',
                    '&::before': {
                        content: "'–'",
                        position: 'absolute',
                        left: -15,
                        bottom: scale(1),
                        [xxl]: {
                            left: -scale(3, true),
                        },
                        [xl]: {
                            left: -15,
                        },
                        [md]: {
                            left: -scale(3, true),
                        },
                        [xs]: {
                            content: 'none',
                        },
                    },
                }}
            >
                <FormField name={nameTo} {...(form === 'rhf' && { ref: endRef })} disabled={disabled}>
                    <CalendarInput
                        placeholder="До"
                        minDate={startDate}
                        calendarProps={{
                            selectedFrom: startDate,
                            selectedTo: endDate,
                        }}
                        placement="bottom-end"
                        {...(form === 'formik' && { ref: endRef })}
                        ref={endRef}
                    />
                </FormField>
            </Layout.Item>
        </Layout>
    );
};
export default CalendarRange;
