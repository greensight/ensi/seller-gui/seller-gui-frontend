import { FieldHelperProps, FieldInputProps, FieldMetaProps } from 'formik';
import { IMaskInput } from 'react-imask';

import { useFieldCSS } from '@scripts/hooks';

import Legend from '../Legend';

export interface MaskProps {
    /** Mask for input */
    mask: RegExp | string | { mask: string; definitions: Record<string, RegExp> };
    /** Formik helpers object (inner) */
    helpers?: FieldHelperProps<string>;
    /** Placeholder for mask */
    placeholderChar?: string;
    /** Is show placeholder */
    lazy?: boolean;
    /** from formik */
    field?: FieldInputProps<any>;
    /** from formik */
    meta?: FieldMetaProps<any>;
    label?: string;
    className?: string;
    blocks?: Record<string, any>;
    format?: (input: any) => string;
    unmask?: (val: string) => any;
    autofix?: boolean;
    overwrite?: boolean;
}

const Mask = ({
    mask,
    label,
    meta,
    field,
    helpers,
    placeholderChar = '_',
    lazy = true,
    className,
    ...props
}: MaskProps) => {
    const value = field?.value || undefined;
    const { basicFieldCSS } = useFieldCSS(meta);
    return (
        <div className={className}>
            <Legend label={label} meta={meta} />
            <IMaskInput
                mask={mask}
                value={value}
                {...props}
                css={basicFieldCSS}
                lazy={lazy}
                placeholderChar={placeholderChar}
                onAccept={(val: string) => {
                    if (val === value) return;

                    if (typeof field?.onChange === 'function') {
                        field.onChange({
                            target: {
                                value: val,
                            },
                        });

                        return;
                    }

                    if (helpers) helpers.setValue(val);
                }}
            />
        </div>
    );
};

export default Mask;
