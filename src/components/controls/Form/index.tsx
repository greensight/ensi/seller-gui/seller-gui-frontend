import { CSSObject } from '@emotion/react';
import {
    FieldHelperProps,
    FieldInputProps,
    FieldMetaProps,
    Formik,
    Form as FormikForm,
    FormikFormProps,
    FormikHelpers,
    FormikProps,
    FormikValues,
} from 'formik';
import { FC, HTMLProps, ReactNode } from 'react';
import * as Yup from 'yup';

import FormFastField from './FastField';
import FormField, { FormFieldProps } from './Field';
import FormMessage, { FormMessageProps } from './Message';
import FormReset, { FormResetProps } from './Reset';
import FormTypedField, { TypedFieldProps } from './TypedField';

export type NumberFieldValue = number | '';

export interface FormCompositionProps<T> {
    Field: FC<FormFieldProps>;
    Message: FC<FormMessageProps>;
    Reset: FC<FormResetProps<T>>;
    FastField: FC<FormFieldProps>;
    FormTypedField: FC<TypedFieldProps>;
}

export type FormProps<T> = Omit<FormikFormProps, 'children'> &
    Omit<HTMLProps<HTMLFormElement>, 'onSubmit' | 'ref' | 'onReset' | 'children'> & {
        /** Initial formik values */
        initialValues: T;
        /** Yup validation schema */
        validationSchema?: Yup.ObjectSchema<any> | (() => Yup.ObjectSchema<any>);
        /** Formik submit handler */
        onSubmit: (values: T, formikHelpers: FormikHelpers<T>) => void | Promise<any>;
        /** Formik reset handler */
        onReset?: (values: T, formikHelpers: FormikHelpers<T>) => void | Promise<any>;
        /** enable reinitialize on initialValues change */
        enableReinitialize?: boolean;
        children?: ReactNode | ReactNode[] | ((props: FormikProps<T>) => ReactNode | ReactNode[]);
    };

export interface FieldProps<T> extends HTMLProps<HTMLInputElement> {
    /** `values` from `useFormikContext` */
    values: FormikValues;
    /** `field` from `useField` */
    field: FieldInputProps<T>;
    /** `meta` from `useField` */
    meta: FieldMetaProps<T>;
    /** `helpers` from `useField` */
    helpers: FieldHelperProps<T>;
    /** Field id. Equals name */
    id?: string;
    /** css object with form field styles */
    css?: CSSObject;
}

export const Form = <T,>({
    initialValues,
    validationSchema,
    onSubmit,
    onReset,
    children,
    enableReinitialize = false,
    ...props
}: FormProps<T> & Partial<FormCompositionProps<T>>) => (
    <Formik
        initialValues={initialValues as any}
        validationSchema={validationSchema}
        onSubmit={onSubmit}
        onReset={onReset}
        enableReinitialize={enableReinitialize}
    >
        {formikProps => (
            <FormikForm noValidate {...props}>
                {typeof children === 'function' ? children(formikProps) : children}
            </FormikForm>
        )}
    </Formik>
);

Form.Field = FormField;
Form.Message = FormMessage;
Form.Reset = FormReset;
Form.FastField = FormFastField;
Form.TypedField = FormTypedField;

export default Form;
