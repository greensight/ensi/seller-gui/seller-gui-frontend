import { FC, HTMLProps } from 'react';

import { colors, scale } from '@scripts/gds';
import { useLinkCSS } from '@scripts/hooks';

export type FooterProps = HTMLProps<HTMLDivElement>;

const Footer: FC<FooterProps> = props => {
    const linkStyles = useLinkCSS('grey');

    return (
        <footer
            css={{
                padding: `${scale(1)}px ${scale(3)}px ${scale(2)}px`,
                marginTop: 'auto',
                textAlign: 'right',
            }}
            {...props}
        >
            <a
                href="https://ensi.tech/"
                target="_blank"
                rel="noreferrer"
                css={{
                    ...linkStyles,
                    position: 'relative',
                    transition: 'opacity 0.2s ease',

                    '&::after': {
                        transition: 'opacity 0.2s ease',
                        content: "''",
                        position: 'absolute',
                        left: 0,
                        bottom: 0,
                        height: '1px',
                        width: '100%',
                        background: colors?.grey800,
                        opacity: 0.2,
                    },

                    '&:hover': {
                        '&::after': {
                            opacity: 1,
                        },
                    },
                }}
            >
                Сделано на Ensi
            </a>
        </footer>
    );
};

export default Footer;
