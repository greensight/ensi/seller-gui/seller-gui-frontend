import { useRouter } from 'next/router';
import { ReactNode, useCallback } from 'react';
import { FieldValues, UseFormReturn } from 'react-hook-form';

import Form, { FormCompositionProps, FormProps } from '@controls/future/Form';

import PageLeaveGuard from '@components/PageLeaveGuard';

import { RedirectMethods } from '@scripts/enums';

interface SubmitResProps<T extends FieldValues> {
    redirectPath: string;
    method: RedirectMethods;
    valuesToReset?: T;
}

interface FormWrapperProps<T extends FieldValues> {
    children: ReactNode | ReactNode[] | ((props: UseFormReturn<T, any>) => ReactNode | ReactNode[]);
    onSubmit: (...args: Parameters<FormProps<T>['onSubmit']>) => SubmitResProps<T> | Promise<SubmitResProps<T>>;
}

const FormWrapper = <T extends FieldValues>({
    children,
    onSubmit,
    ...props
}: Omit<FormProps<T>, 'onSubmit'> & Partial<FormCompositionProps> & FormWrapperProps<T>) => {
    const { push, replace } = useRouter();

    const submitForm = useCallback(
        async (v:  T, h: UseFormReturn<T, any> | null) => {
            try {
                const res = await onSubmit(v, h!);
                h!.reset(res.valuesToReset || v);
                setTimeout(() => {
                    if (res.redirectPath && res.method === RedirectMethods.push) push({ pathname: res.redirectPath });
                    if (res.redirectPath && res.method === RedirectMethods.replace)
                        replace({ pathname: res.redirectPath });
                }, 0);
                // eslint-disable-next-line no-empty
            } catch (_) {
                console.error(_);
            }
        },
        [onSubmit, push, replace]
    );

    return (
        <Form {...props} onSubmit={submitForm}>
            {typeof children === 'function' ? (
                params => (
                    <>
                        <PageLeaveGuard onSubmit={submitForm} />
                        {children(params)}
                    </>
                )
            ) : (
                <>
                    <PageLeaveGuard onSubmit={submitForm} />
                    {children}
                </>
            )}
        </Form>
    );
};

export default FormWrapper;
