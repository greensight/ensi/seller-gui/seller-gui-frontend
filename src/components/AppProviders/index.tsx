import { QueryClient, QueryClientProvider } from '@tanstack/react-query';
import { ReactQueryDevtools } from '@tanstack/react-query-devtools';
import { ReactNode } from 'react';

import { AuthProvider } from '@context/auth';
import { BodyScrollLockProvider } from '@context/bodyScrollLock';
import { CommonProvider } from '@context/common';
import { ModalProvider } from '@context/modal';

import { STALE_TIME } from '@scripts/constants';
import { ThemeProvider, theme } from '@scripts/gds';

const queryClient = new QueryClient({
    defaultOptions: {
        queries: {
            staleTime: STALE_TIME,
            retry: 0,
            refetchOnWindowFocus: process.env.NODE_ENV === 'production',
        },
    },
});

const AppProviders = ({ children, pageProps }: { children: ReactNode; pageProps?: any }) => (
    <ThemeProvider theme={theme}>
        <QueryClientProvider client={queryClient}>
            <CommonProvider>
                <BodyScrollLockProvider>
                    <ModalProvider>
                        <AuthProvider state={pageProps}>{children}</AuthProvider>
                    </ModalProvider>
                </BodyScrollLockProvider>
            </CommonProvider>
            {process.env.NODE_ENV !== 'production' && <ReactQueryDevtools initialIsOpen={false} position="bottom-right" />}
        </QueryClientProvider>
    </ThemeProvider>
);

export default AppProviders;
