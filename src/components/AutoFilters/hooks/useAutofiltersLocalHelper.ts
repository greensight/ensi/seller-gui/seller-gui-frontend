import { useRouter } from 'next/router';
import { useCallback, useEffect, useMemo } from 'react';

import { MetaField } from '@api/common/types';

import { useSessionStorage } from '@scripts/hooks';

import { AutofiltersLocalHelper } from '../types';
import { usePopupHelper } from './usePopupHelper';

export const useAutofiltersLocalHelper = ({
    meta,
    manualHiddenFilters,
    filtersSettingsName,
    queryPart,
    onResetFilters,
}: AutofiltersLocalHelper) => {
    const {
        push,
        pathname,
        query: { tab, id },
    } = useRouter();

    const filters = useMemo(
        () => meta?.fields.filter(f => f.filter && !manualHiddenFilters?.includes(f.code)),
        [manualHiddenFilters, meta?.fields]
    );

    const filtersObject = useMemo(
        () =>
            filters?.reduce(
                (acc, filter) => {
                    acc[filter.code] = filter.name;
                    return acc;
                },
                {} as Record<string, string>
            ) || {},
        [filters]
    );

    const [filtersSettings, setFiltersSettings] = useSessionStorage<string[]>(
        filtersSettingsName || `${pathname}FilterSettings`,
        []
    );

    useEffect(() => {
        if (filtersSettings.length === 0 && meta?.default_filter) setFiltersSettings(meta?.default_filter);
    }, [meta?.default_filter, filtersSettings, setFiltersSettings]);

    const filtersToShow = useMemo(
        () =>
            filtersSettings.reduce((acc, settingName) => {
                const foundFilter = filters?.find(f => f.code === settingName);
                if (foundFilter && !manualHiddenFilters?.includes(foundFilter?.code)) acc.push(foundFilter);
                return acc;
            }, [] as MetaField[]),
        [filters, filtersSettings, manualHiddenFilters]
    );

    const resetFormHelper = useCallback(() => {
        if (onResetFilters) onResetFilters();
        else
            push({
                pathname,
                query: {
                    ...queryPart,
                    ...(tab && { tab, ...(id && { id }) }),
                },
            });
    }, [id, onResetFilters, pathname, push, queryPart, tab]);

    const { isOpen, openHandler, closeHandler } = usePopupHelper();

    const filtersDrawerProps = { isOpen, closeHandler, filtersSettings, setFiltersSettings, filters, filtersObject };

    return { filtersToShow, filtersSettings, resetFormHelper, openHandler, filtersDrawerProps };
};
