import { useCallback, useState } from 'react';

export const usePopupHelper = () => {
    const [isOpen, setIsOpen] = useState(false);

    const openHandler = useCallback(() => setIsOpen(true), []);
    const closeHandler = useCallback(() => setIsOpen(false), []);

    return { isOpen, setIsOpen, openHandler, closeHandler };
};
