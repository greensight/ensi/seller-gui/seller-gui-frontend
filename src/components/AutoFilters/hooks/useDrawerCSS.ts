import { CSSObject } from '@emotion/react';

import { colors, scale } from '@scripts/gds';

export const useDrawerCSS = () => {
    const formStyles: CSSObject = { display: 'flex', flexDirection: 'column', height: '100%' };
    const ulStyles: CSSObject = { position: 'relative' };
    const liStyles: CSSObject = {
        display: 'flex',
        alignItems: 'center',
        gap: scale(1),
        padding: `${scale(1)}px 0`,
        svg: { opacity: 0 },
        '&:hover': { svg: { opacity: 1 } },
        backgroundColor: colors.white,
        left: `${scale(3)}px !important`,
    };

    return { formStyles, ulStyles, liStyles };
};
