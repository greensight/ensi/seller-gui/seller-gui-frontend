export const booleanOptions = [
    { key: '1', content: 'Нет', value: '0' },
    { key: '2', content: 'Да', value: '1' },
];

const delimeter = '____';
export const encodeFieldName = (fieldName: string) => fieldName.replaceAll('.', delimeter);
export const decodeFieldName = (fieldName: string) => fieldName.replaceAll(delimeter, '.');
