import { UseQueryResult } from '@tanstack/react-query';
import { ReactNode } from 'react';

import { CommonResponse, FetchError, Meta } from '@api/common/types';

import { TableColumnDefAny, TooltipItem } from '@components/Table';

import { autoFilterData } from '@hooks/autoTable';

import { AccessMatrix, ReturnedMatrix } from '@scripts/hooks';

export interface ListBuilderProps<EntityType> {
    /**
     * Название списка
     * @example 'Признаки'
     */
    title: string;
    /**
     * Дочерние компоненты
     */
    children?: ReactNode;
    /**
     * Матрица доступа
     * @example { LIST: { view: true, }, ID: { view: true, delete: true, edit: true, create: true, }, }
     */
    access?: ReturnedMatrix<AccessMatrix>;
    /**
     * Хук ендпоинта поиска сущности для списка
     * @example useReviews
     */
    searchHook: (
        data: Record<string, any>,
        enabled?: boolean
    ) => UseQueryResult<CommonResponse<EntityType[]>, FetchError>;
    /**
     * Хук обогащения даты для поиска сущности, например, мидлвара для полей и тд
     */
    enrichSearchHookRequest?: (data: autoFilterData) => Record<string, any>;
    /**
     * Хук ендпоинта для меты сущности
     * @example useReviewsMeta
     */
    metaHook: (enabled?: boolean) => UseQueryResult<
        {
            data: Meta;
        },
        FetchError
    >;
    /**
     * Хук для обогащения мета даты сущности
     */
    enrichMetaData?: (metaData?: Meta) => Partial<Meta>;
    /**
     * Флаг загрузки дополнительных операций: удаление, редактирование и пр.
     * @example true
     */
    isLoading?: boolean;
    /**
     * Дополнительные колонки
     * @example [columnHelper.accessor('name', {
     *              header: 'Наименование',
     *              cell: props => props.getValue(),
     *              enableHiding: true,
     *          })]
     */
    extraColumns?: TableColumnDefAny[];
    /**
     * Туллтип для строк в списке
     * @example [{
     *              type: 'edit',
     *              text: 'Задать признаки',
     *              action: tooltipAction,
     *          }]
     */
    tooltipItems?: TooltipItem[];
    /**
     * Дополнительные компоненты в хедере для массовых операций
     */
    headerInner?: (table?: any) => JSX.Element | null;
    /**
     * Туллтипы для массовых операций/действий
     * @example [{
     *              type: 'edit',
     *              text: 'Задать признаки',
     *              action: tooltipAction,
     *          }]
     */
    tooltipForAdditionalActions?: TooltipItem[];
}
