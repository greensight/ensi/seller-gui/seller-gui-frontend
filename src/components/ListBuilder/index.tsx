import Link from 'next/link';
import { useRouter } from 'next/router';
import { useCallback, useMemo } from 'react';

import { useError } from '@context/modal';

import Tooltip, { ContentBtn, hideOnEsc } from '@controls/Tooltip';

import AutoFilters from '@components/AutoFilters';
import Block from '@components/Block';
import { ListBuilderProps } from '@components/ListBuilder/types';
import PageWrapper from '@components/PageWrapper';
import Table, { TableColumnDefAny, TrProps, useSorting, useTable } from '@components/Table';
import { getSelectColumn, getSettingsColumn } from '@components/Table/columns';
import { RowTooltipWrapper, TableEmpty, TableFooter, TableHeader } from '@components/Table/components';

import { ITEMS_PER_PRODUCTS_PAGE } from '@scripts/constants';
import { Button, scale } from '@scripts/gds';
import { declOfNum, getPagination, getTotal, getTotalPages } from '@scripts/helpers';
import { useRedirectToNotEmptyPage, useTableList } from '@scripts/hooks';
import { useAutoFilters, useAutoFutureColumns, useAutoTableData } from '@scripts/hooks/autoTable';

import PlusIcon from '@icons/plus.svg';
import KebabIcon from '@icons/small/kebab.svg';

export const initialSort = { id: 'id', desc: true };

export const ListBuilder = <EntityType extends Record<string, any>>({
    title,
    children,
    access,
    searchHook,
    enrichSearchHookRequest,
    metaHook,
    enrichMetaData,
    isLoading,
    extraColumns,
    tooltipItems,
    headerInner,
    tooltipForAdditionalActions,
}: ListBuilderProps<EntityType>) => {
    const { pathname, push } = useRouter();

    const { data: metaData, error: metaError, isLoading: isMetaLoading } = metaHook(access ? !!access.LIST.view : true);
    useError(metaError);
    const meta = useMemo(
        () => (metaData && { ...metaData?.data, ...(enrichMetaData && enrichMetaData(metaData.data)) }) || undefined,
        [metaData]
    );

    const [{ backendSorting }, sortingPlugin] = useSorting<EntityType>(pathname, initialSort);

    const autoFiltersData = useAutoFilters(meta);
    const {
        metaField,
        values,
        filtersActive,
        URLHelper,
        reset,
        searchRequestFilter,
        emptyInitialValues,
        searchRequestIncludes,
        codesToSortKeys,
        clearInitialValue,
    } = autoFiltersData;

    const { activePage, itemsPerPageCount, setItemsPerPageCount } = useTableList({
        defaultSort: meta?.default_sort,
        defaultPerPage: ITEMS_PER_PRODUCTS_PAGE,
        codesToSortKeys,
        pageKey: pathname,
    });

    const {
        data,
        isFetching: isSearchEntityLoading,
        error,
    } = searchHook(
        {
            include: searchRequestIncludes,
            sort: backendSorting,
            filter: searchRequestFilter,
            pagination: getPagination(activePage, itemsPerPageCount),
            ...(enrichSearchHookRequest && enrichSearchHookRequest(autoFiltersData)),
        },
        Boolean(metaData) && (access?.LIST.view as boolean)
    );
    useError(error);

    const total = getTotal(data);
    const totalPages = getTotalPages(data, itemsPerPageCount);

    useRedirectToNotEmptyPage({ activePage, itemsPerPageCount, total, pageKey: pathname });

    const columns = useAutoFutureColumns(meta);
    const rows = useAutoTableData<EntityType>(data?.data, metaField);

    const mergeColumns = (
        columns: TableColumnDefAny[],
        extraColumns: TableColumnDefAny[] | undefined
    ): TableColumnDefAny[] => {
        if (!extraColumns) return columns;

        const result = [...columns];
        extraColumns.forEach(extraColumn => {
            const index = columns.findIndex(column => column.accessorKey === extraColumn.accessorKey);
            if (index !== -1) {
                result[index] = extraColumn;
            }
        });
        return result;
    };

    const table = useTable(
        {
            data: rows,
            columns: [
                ...(headerInner && getSelectColumn(undefined, pathname) ? [getSelectColumn(undefined, pathname)] : []),
                ...mergeColumns(columns, extraColumns),
                getSettingsColumn({
                    visibleColumns: meta?.default_list,
                    tooltipContent: tooltipItems,
                }),
            ],
            meta: {
                tableKey: pathname,
            },
        },
        [sortingPlugin]
    );

    const getTooltipForRow = useCallback(() => tooltipItems || [], [tooltipItems]);
    const renderRow = useCallback(
        ({ children, ...props }: TrProps<any>) => (
            <RowTooltipWrapper
                {...props}
                getTooltipForRow={getTooltipForRow}
                onDoubleClick={() => {
                    push(`${pathname}/${props.row?.original[meta?.detail_link || 'id']}`);
                }}
            >
                {children}
            </RowTooltipWrapper>
        ),
        [getTooltipForRow]
    );

    const isLoadingCommon = isLoading || isMetaLoading || isSearchEntityLoading;

    return (
        <PageWrapper h1={title} isLoading={isLoadingCommon}>
            <AutoFilters
                initialValues={values}
                emptyInitialValues={emptyInitialValues}
                onSubmit={URLHelper}
                filtersActive={filtersActive}
                css={{ marginBottom: scale(2) }}
                meta={meta}
                onResetFilters={reset}
                clearInitialValue={clearInitialValue}
            />
            <Block>
                <Block.Body>
                    <TableHeader css={{ justifyContent: 'space-between' }}>
                        {table.getSelectedRowModel().flatRows.length > 0 ? (
                            <p>
                                Выделено: <b>{table.getSelectedRowModel().flatRows.length}</b>
                            </p>
                        ) : (
                            <p>Найдено {`${total} ${declOfNum(total, ['запись', 'записи', 'записей'])}`}</p>
                        )}
                        <div css={{ display: 'flex' }}>
                            {headerInner && headerInner(table)}
                            {tooltipForAdditionalActions && (
                                <Tooltip
                                    content={
                                        <>
                                            {tooltipForAdditionalActions.map(t => (
                                                <ContentBtn
                                                    key={t.text}
                                                    type={t.type}
                                                    onClick={e => {
                                                        e.stopPropagation();
                                                        t.action(table.getSelectedRowModel().flatRows);
                                                    }}
                                                    disabled={
                                                        typeof t?.isDisable === 'function'
                                                            ? t.isDisable()
                                                            : t?.isDisable
                                                    }
                                                >
                                                    {t.text}
                                                </ContentBtn>
                                            ))}
                                        </>
                                    }
                                    plugins={[hideOnEsc]}
                                    trigger="click"
                                    arrow
                                    theme="light"
                                    placement="bottom"
                                    minWidth={scale(36)}
                                    disabled={tooltipForAdditionalActions.length === 0}
                                    appendTo={() => document.body}
                                >
                                    <Button
                                        theme="outline"
                                        Icon={KebabIcon}
                                        css={{ marginLeft: scale(2), marginRight: scale(2) }}
                                        iconAfter
                                    >
                                        Действия
                                    </Button>
                                </Tooltip>
                            )}
                            {(access?.LIST.create || access === undefined) && !headerInner && (
                                <Link href={`${pathname}/create`} passHref>
                                    <Button Icon={PlusIcon} as="a">
                                        Создать
                                    </Button>
                                </Link>
                            )}
                        </div>
                    </TableHeader>
                    <Table instance={table} {...(tooltipItems && { Tr: renderRow })} />
                    {rows.length === 0 ? (
                        <TableEmpty
                            onResetFilters={reset}
                            filtersActive={filtersActive}
                            titleWithFilters="Записи не найдены"
                            titleWithoutFilters="Нет записей"
                            addItems={() => push(`${pathname}/create`)}
                        />
                    ) : (
                        <TableFooter
                            pages={totalPages}
                            itemsPerPageCount={itemsPerPageCount}
                            setItemsPerPageCount={setItemsPerPageCount}
                            pageKey={pathname}
                        />
                    )}
                </Block.Body>
            </Block>

            {/* for additions popups and other components */}
            {children}
        </PageWrapper>
    );
};

export default ListBuilder;
