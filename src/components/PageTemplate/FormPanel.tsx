import { ReactNode } from 'react';
import { FieldValues, UseFormReturn, useFormContext } from 'react-hook-form';

export interface FormPanelProps {
    children: ReactNode | (({ formState }: UseFormReturn<FieldValues>) => ReactNode);
}
const FormPanel = ({ children }: FormPanelProps) => {
    const formContext = useFormContext();

    return typeof children === 'function' ? children(formContext)! : children!;
};

export default FormPanel;
