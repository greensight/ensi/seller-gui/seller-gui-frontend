import { useFormikContext } from 'formik';
import type { ParsedUrlQuery } from 'querystring';
import { useRef } from 'react';
import { UseFormReturn, useFormContext } from 'react-hook-form';

import { useModalsContext } from '@context/modal';

import ActionPopup, { ActionState } from '@components/ActionPopup';
import useForm from '@components/controls/future/Form/useForm';

import { ActionType } from '@scripts/enums';
import { usePopupState } from '@scripts/hooks';

import { useNavigationObserver } from './hook';

type ValueOrPromiseType<T> = T | Promise<T>;
type UrlOrStringType = { pathname: string; query: ParsedUrlQuery } | string;
type SubmitUrlType = void | UrlOrStringType | boolean;

interface IPageLeaveGuardProps<T extends Record<string, any>> {
    isTrackParameters?: boolean;
    onSubmit?: (values: T, formProps: UseFormReturn<T, any> | null) => ValueOrPromiseType<SubmitUrlType>;
}

const useIsomorphicForm = (): Pick<
    ReturnType<typeof useFormikContext>,
    'dirty' | 'submitForm' | 'validateForm' | 'resetForm' | 'isSubmitting'
> & {
    isSubmitting: boolean;
    getValues(): any;
    markErrorFields(): void;
    context: UseFormReturn<any, any> | null;
} => {
    const oldConsoleWarning = console.warn;
    const oldConsoleError = console.error;
    console.warn = () => {};
    console.error = () => {};
    const formikContext = useFormikContext();
    const formContext = useFormContext();
    const form = useForm()!;
    console.warn = oldConsoleWarning;
    console.error = oldConsoleError;

    if (!formikContext && !formContext) {
        throw new Error('PageLeaveGuard can only exist as a descendant of formik or react-hook-form');
    }

    if (formContext)
        return {
            isSubmitting: formContext.formState.isSubmitting,
            dirty: formContext.formState.isDirty && Object.keys(formContext.formState.dirtyFields).length > 0,
            resetForm: () => formContext.reset(),
            submitForm: () =>
                new Promise(resolve => {
                    if (form) form.onSubmitHandler(formContext.getValues());
                    resolve(formContext.getValues());
                }),
            validateForm: async () => formContext.trigger().then(() => formContext?.formState.errors),
            getValues: formContext.getValues,
            markErrorFields: () =>
                Object.keys(formContext.getValues()).forEach(fieldName => formContext.trigger(fieldName)),
            context: formContext,
        };

    return {
        ...formikContext,
        getValues() {
            return () => formikContext.values;
        },
        markErrorFields: () =>
            formikContext.setTouched(
                Object.keys(formikContext.values as object).reduce(
                    (acc, key) => {
                        acc[key] = true;
                        return acc;
                    },
                    {} as Record<string, boolean>
                )
            ),
        context: null,
    };
};

const PageLeaveGuard = <T extends Record<string, any>>({
    // eslint-disable-next-line @typescript-eslint/no-unused-vars
    isTrackParameters = false,
    onSubmit,
}: IPageLeaveGuardProps<T>) => {
    const [popupState, popupDispatch] = usePopupState<Partial<ActionState & { isLoading: boolean }>>({
        open: false,
        isLoading: false,
    });

    const { dirty, submitForm, validateForm, resetForm, getValues, markErrorFields, context, isSubmitting } =
        useIsomorphicForm();

    const dirtyRef = useRef<boolean>();
    const submittingRef = useRef<boolean>();
    dirtyRef.current = dirty;
    submittingRef.current = isSubmitting;

    const shouldStopNavigation = dirty && !isSubmitting;

    const { cancelNavigation, confirmNavigation } = useNavigationObserver({
        shouldStopNavigation,
        onNavigate() {
            popupDispatch({ type: ActionType.Add });
        },
    });

    const { appendModal } = useModalsContext();

    return (
        <ActionPopup
            open={!!popupState.open}
            onAction={async () => {
                markErrorFields();
                const errors = await validateForm();
                if (Object.keys(errors).length) {
                    appendModal({
                        title: 'Невозможно сохранить',
                        message: 'Устраните ошибки в форме',
                        theme: 'error',
                    });
                    popupDispatch({ type: ActionType.PreClose });
                    cancelNavigation();
                    return;
                }

                try {
                    popupDispatch({ type: ActionType.Edit, payload: { ...popupState, isLoading: true } });
                    if (onSubmit) {
                        await onSubmit(getValues(), context);
                    } else {
                        await submitForm();
                    }
                } catch (e) {
                    console.error(e);
                    return;
                } finally {
                    popupDispatch({ type: ActionType.Edit, payload: { ...popupState, isLoading: false } });
                }

                resetForm();
                popupDispatch({ type: ActionType.PreClose });
                setTimeout(() => {
                    confirmNavigation();
                }, 0);
            }}
            onBackdropClick={() => {
                popupDispatch({ type: ActionType.PreClose });
                cancelNavigation();
            }}
            onClose={() => {
                resetForm({
                    values: getValues(),
                });
                popupDispatch({ type: ActionType.PreClose });
                confirmNavigation();
            }}
            onUnmount={() => {
                popupDispatch({ type: ActionType.Close });
            }}
            disableAction={popupState.isLoading}
            disableClose={popupState.isLoading}
            leftAddonIconTheme="warning"
            title="Сохранить изменения?"
            blockButtons
        >
            <ActionPopup.Content>
                <p>Вы внесли изменения на странице, но не сохранили их</p>
            </ActionPopup.Content>
        </ActionPopup>
    );
};

export default PageLeaveGuard;
