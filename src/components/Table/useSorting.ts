import { ColumnSort, RowData, SortingState, getSortedRowModel } from '@tanstack/react-table';
import { SetStateAction, useCallback, useEffect, useMemo, useState } from 'react';
import deepEqual from 'react-fast-compare';

import { usePrevious, useSessionStorage } from '@scripts/hooks';

import { TablePlugin } from './types';

export const useSorting = <TData extends RowData>(key: string, initialSort?: ColumnSort, reinitialize = false) => {
    const [savedSort, setSavedSort] = useSessionStorage(`${key}Sort`, () => (initialSort ? [initialSort] : []));
    const [sorting, setSorting] = useState<SortingState>(savedSort);

    const prevInitialSort = usePrevious(initialSort);
    useEffect(() => {
        if (!reinitialize || !initialSort || deepEqual(prevInitialSort, initialSort)) return;

        setSorting([initialSort]);
    }, [initialSort, prevInitialSort, reinitialize]);

    const backendSorting = useMemo(() => sorting.map(e => (e.desc ? `-${e.id}` : `${e.id}`)), [sorting]);

    const onSortingChange = useCallback(
        (newSort: SetStateAction<SortingState>) => {
            setSavedSort(newSort);
            setSorting(newSort);
        },
        [setSavedSort]
    );

    const invertedSorting = useMemo(
        () =>
            sorting.map(e => ({
                ...e,
                desc: !e.desc,
            })),
        [sorting]
    );

    return [
        {
            sorting,
            invertedSorting,
            // eslint-disable-next-line no-nested-ternary
            backendSorting: backendSorting.length ? backendSorting : initialSort?.id ? [initialSort.id] : [],
        },
        {
            state: { sorting },
            root: {
                enableSorting: true,
                onSortingChange,
                getSortedRowModel: getSortedRowModel(),
            },
        } as TablePlugin<TData>,
    ] as const;
};
