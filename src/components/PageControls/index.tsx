import { useFormikContext } from 'formik';
import { useRouter } from 'next/router';
import { Children, ReactNode, cloneElement, isValidElement } from 'react';
import { useFormContext } from 'react-hook-form';

import { CREATE_PARAM } from '@scripts/constants';
import { Layout, type LayoutProps, scale } from '@scripts/gds';
import { AccessMatrix, ReturnedMatrix } from '@scripts/hooks';

import { PageControlsApply } from './Apply';
import { PageControlsButton } from './Button';
import { PageControlsClose } from './Close';
import { PageControlsDelete } from './Delete';
import { PageControlsSave } from './Save';
import { PageControlChildProps } from './types';

interface PageControlsCompositionProps {
    Button: typeof PageControlsButton;
    Delete: typeof PageControlsDelete;
    Close: typeof PageControlsClose;
    Apply: typeof PageControlsApply;
    Save: typeof PageControlsSave;
}

export type PageControlsProps = Pick<LayoutProps, 'gap'> & {
    children: ReactNode | ReactNode[];

    access?: ReturnedMatrix<AccessMatrix>;
};

const idKeys = ['entity_id', 'id'];

const useIsomorphicDirty = (): boolean => {
    const oldConsoleWarning = console.warn;
    console.warn = () => {};
    const formikContext = useFormikContext();
    console.warn = oldConsoleWarning;
    const formContext = useFormContext();

    if (!formikContext && !formContext) {
        throw new Error('PageControls can only exist as a descendant of formik or react-hook-form');
    }

    if (formContext) return formContext.formState.isDirty && !!Object.keys(formContext.formState.dirtyFields).length;

    return formikContext.dirty;
};

const useIsCreationPage = () => {
    const { query } = useRouter();

    const idKey = idKeys.find(e => !!query[e]);

    return idKey ? query[idKey] === CREATE_PARAM : false;
};

const PageControls = ({ children, access, gap = scale(1) }: PageControlsProps) => {
    const isCreationPage = useIsCreationPage();
    const isDirty = useIsomorphicDirty();

    const canDelete = access?.ID.delete;

    return (
        <Layout type="flex" gap={gap}>
            {Children.map(children, child => {
                if (!isValidElement<PageControlChildProps>(child)) return null;

                return cloneElement<any>(child, {
                    ...child.props,
                    isDirty,
                    isCreationPage,
                    canDelete,
                });
            })}
        </Layout>
    );
};

PageControls.Button = PageControlsButton;
PageControls.Delete = PageControlsDelete;
PageControls.Close = PageControlsClose;
PageControls.Save = PageControlsSave;
PageControls.Apply = PageControlsApply;

export default PageControls as typeof PageControls & PageControlsCompositionProps;
