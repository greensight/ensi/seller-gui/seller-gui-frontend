export interface PageControlChildProps {
    isDirty?: boolean;
    isCreationPage?: boolean;

    canDelete?: boolean;
}
