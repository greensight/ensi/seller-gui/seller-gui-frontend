import CheckIcon from '@icons/small/check.svg';

import { PageControlsButton, PageControlsButtonProps } from './Button';
import { PageControlChildProps } from './types';

export interface PageControlsSaveProps
    extends Omit<PageControlsButtonProps, 'type' | 'dirtyOnly' | 'children'>,
        PageControlChildProps {}

export const PageControlsSave = ({ onClick, Icon = CheckIcon, iconAfter = true, ...props }: PageControlsSaveProps) => (
    <PageControlsButton Icon={Icon} iconAfter={iconAfter} onClick={onClick} {...props} dirtyOnly type="submit">
        Сохранить
    </PageControlsButton>
);
