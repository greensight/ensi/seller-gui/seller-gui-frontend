import { PageControlsButton, PageControlsButtonProps } from './Button';
import { PageControlChildProps } from './types';

export interface PageControlsCloseProps extends Pick<PageControlsButtonProps, 'onClick'>, PageControlChildProps {}

export const PageControlsClose = ({ onClick, ...props }: PageControlsCloseProps) => {
    delete props.isDirty;
    delete props.canDelete;
    delete props.isCreationPage;

    return (
        <PageControlsButton theme="secondary" onClick={onClick}>
            Закрыть
        </PageControlsButton>
    );
};
