import { PageControlsButton, PageControlsButtonProps } from './Button';
import { PageControlChildProps } from './types';

export interface PageControlsDeleteProps extends Pick<PageControlsButtonProps, 'onClick'>, PageControlChildProps {}

export const PageControlsDelete = ({ canDelete, isCreationPage, onClick, ...props }: PageControlsDeleteProps) => {
    delete props.isDirty;

    return !isCreationPage && canDelete ? (
        <PageControlsButton theme="dangerous" onClick={onClick}>
            Удалить
        </PageControlsButton>
    ) : null;
};
