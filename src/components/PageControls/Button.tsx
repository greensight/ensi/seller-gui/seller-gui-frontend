import { Button, type ButtonProps, Layout } from '@scripts/gds';

import { PageControlChildProps } from './types';

export interface PageControlsButtonProps extends ButtonProps<'button'>, PageControlChildProps {
    // Включать кнопку только если в форме есть изменения
    dirtyOnly?: boolean;
}

export const PageControlsButton = ({ children, dirtyOnly, isDirty, ...props }: PageControlsButtonProps) => {
    delete props.canDelete;
    delete props.isCreationPage;

    return (
        <Layout.Item>
            <Button {...props} {...(dirtyOnly && !isDirty && { disabled: true })}>
                {children}
            </Button>
        </Layout.Item>
    );
};
