import { useQuery } from '@tanstack/react-query';

import { CommonResponse } from '@api/common/types';
import { useAuthApiClient } from '@api/hooks/useAuthApiClient';

import { STALE_TIME_DAY } from '@scripts/constants';

import { FetchError } from '../index';

export const useMenu = () => {
    const apiClient = useAuthApiClient();

    return useQuery<CommonResponse<string[]>, FetchError>({
        queryKey: ['menu'],
        queryFn: () => apiClient.get('menu'),
        staleTime: STALE_TIME_DAY,
    });
};
