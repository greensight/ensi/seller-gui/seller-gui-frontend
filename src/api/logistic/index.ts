import { useQuery } from '@tanstack/react-query';
import { useCallback } from 'react';

import { CommonResponse, FetchError } from '@api/common/types';
import { useAuthApiClient } from '@api/hooks/useAuthApiClient';
import { DeliveryService, DeliveryServicesData } from '@api/logistic/types';

const LOGISTIC_URL = 'logistic/delivery-services';

export const useDeliveryServices = (data: DeliveryServicesData = {}) => {
    const apiClient = useAuthApiClient();

    return useQuery<CommonResponse<DeliveryService[]>, FetchError>({
        queryKey: ['delivery-services', data],
        queryFn: () => apiClient.post(`${LOGISTIC_URL}:search`, { data }),
    });
};

export const useGetDeliveryServicesAutocomplete = () => {
    const apiClient = useAuthApiClient();

    const deliveryServicesSearchFn = useCallback(
        async (query: string) => {
            const res = await apiClient.post(`${LOGISTIC_URL}:search`, {
                data: {
                    filter: {
                        name_like: query,
                    },
                },
            });

            return {
                options: res.data.map((service: { name: string; id: string }) => ({
                    key: service.name,
                    value: service.id,
                })),
                hasMore: false,
            };
        },
        [apiClient]
    );

    const deliveryServicesOptionsByValuesFn = useCallback(
        async (vals: string[], abortController: AbortController) => {
            const res = await apiClient.post(`${LOGISTIC_URL}:search`, {
                data: { filter: { id: vals }, abortController },
            });

            return res.data.map((i: { name: string; id: string }) => ({
                key: i.name,
                value: i.id,
            }));
        },
        [apiClient]
    );

    return { deliveryServicesSearchFn, deliveryServicesOptionsByValuesFn };
};
