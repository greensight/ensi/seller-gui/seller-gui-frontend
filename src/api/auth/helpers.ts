export interface LoginData {
    data: {
        accessToken: string;
        hasRefreshToken?: boolean;
        expiresAt: string;
    };
}
