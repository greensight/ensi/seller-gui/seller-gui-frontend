import { useMutation, useQuery, useQueryClient } from '@tanstack/react-query';

import { useAuthApiClient } from '@api/hooks/useAuthApiClient';
import { GetSellerStatusesResponse, SellerForPatch, SellerResponse } from '@api/settings/types';
import { FetchError } from '..';

export const QueryKeys = {
    getSellerDetail: () => ['get-seller-detail'],
    getSellerStatuses: () => ['get-seller-statuses'],
};

/** Получение объекта типа Seller */
export function useSellerDetail(enabled = true) {
    const apiClient = useAuthApiClient();
    return useQuery<SellerResponse, FetchError>({
        queryKey: QueryKeys.getSellerDetail(),
        queryFn: () => apiClient.get(`units/sellers`),
        enabled,
    });
}

/** Частичное изменение объекта типа Seller */
export const usePatchSeller = () => {
    const queryClient = useQueryClient();

    const apiClient = useAuthApiClient();
    return useMutation<SellerResponse, FetchError, SellerForPatch>(
        ({ ...data }) => apiClient.patch(`units/sellers`, { data }),
        {
            onSuccess: () => {
                queryClient.invalidateQueries(QueryKeys.getSellerDetail());
            },
        }
    );
};

/** Получение объектов типа SellerStatus */
export function useSellerStatuses(enabled = true) {
    const apiClient = useAuthApiClient();
    return useQuery<GetSellerStatusesResponse, FetchError>({
        queryKey: QueryKeys.getSellerStatuses(),
        queryFn: () => apiClient.get('units/sellers/seller-statuses', {}),
        enabled,
    });
}
