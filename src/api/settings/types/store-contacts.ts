import { Prettify, CommonResponse } from '@api/common/types';

export interface StoreContactReadonlyProperties {
    /**
     * Идентификатор контактного лица для склада
     * @example 1
     */
    id: number;
    /**
     * Время создания контактного лица для склада
     * @example "2021-01-15T14:55:35.000000Z"
     */
    created_at: string;
    /**
     * Время обновления контактного лица для склада
     * @example "2021-01-15T14:55:35.000000Z"
     */
    updated_at: string;
}

export interface StoreContactFillableProperties {
    /**
     * ID склада
     * @example 17
     */
    store_id: number;
    /**
     * Имя контактного лица
     * @example "Иванов Иван Иванович"
     */
    name: string;
    /**
     * Телефон контактного лица
     * @example "+79876543210"
     */
    phone: string;
    /**
     * Email контактного лица
     * @example "ivanov@example.com"
     */
    email: string;
}

export type StoreContactRequiredProperties = Record<string, any>;

/**
 * Создание контактного лица склада
 */
export type StoreContactForCreate = Prettify<StoreContactFillableProperties & StoreContactRequiredProperties>;
export type StoreContactResponse = CommonResponse<StoreContactReadonlyProperties & StoreContactFillableProperties>;

/**
 * Частичное изменение объекта типа StoreContact
 */
export type StoreContactForPatch = Prettify<StoreContactFillableProperties>;
