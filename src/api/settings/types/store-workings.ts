import { CommonResponse, Prettify } from '@api/common/types';

export interface StoreWorkingReadonlyProperties {
    /**
     * Идентификатор записи о времени работы
     * @example 1
     */
    id: number;
    /**
     * Время создания записи о времени работы
     * @example "2021-01-15T14:55:35.000000Z"
     */
    created_at: string;
    /**
     * Время обновления записи о времени работы
     * @example "2021-01-15T14:55:35.000000Z"
     */
    updated_at: string;
}

export interface StoreWorkingFillableProperties {
    /**
     * ID склада
     * @example 17
     */
    store_id: number;
    /**
     * Флаг активности дня работы склада
     */
    active: boolean;
    /**
     * День недели (1-7)
     * @example 1
     */
    day: number;
    /**
     * Время начала работы склада (00:00)
     * @example "00:00"
     */
    working_start_time: string;
    /**
     * Время конца работы склада (00:00)
     * @example "00:00"
     */
    working_end_time: string;
}

export type StoreWorkingRequiredProperties = Record<string, any>;
/**
 * Создание времени работы склада */
export type StoreWorkingForCreate = Prettify<StoreWorkingFillableProperties & StoreWorkingRequiredProperties>;
export type StoreWorkingResponse = CommonResponse<StoreWorkingReadonlyProperties & StoreWorkingFillableProperties>;

/**
 * Частичное изменение объекта типа StoreWorking */

export type StoreWorkingForPatch = Prettify<StoreWorkingFillableProperties>;
