export interface Address {
    /**
     * Полная строка адреса
     * @example "107140, г Москва, Красносельский р-н, ул Красносельская Верхн., д 3А"
     */
    address_string: string;
    /**
     * Код страны
     * @example "RU"
     */
    country_code?: string;
    /**
     * Почтовый индекс
     * @example "124482"
     */
    post_index?: string;
    /**
     * Регион
     * @example "Москва"
     */
    region?: string;
    /**
     * GUID региона
     * @example "0c5b2444-70a0-4932-980c-b4dc0d3f02b5"
     */
    region_guid?: string;
    /**
     * название области
     * @example "Центральный"
     */
    area?: string;
    /**
     * GUID области
     */
    area_guid?: string;
    /**
     * название города
     * @example "Зеленоград"
     */
    city?: string;
    /**
     * GUID города
     * @example "ec44c0ee-bf24-41c8-9e1c-76136ab05cbf"
     */
    city_guid?: string;
    /**
     * улица
     * @example "ул Красносельская Верхн."
     */
    street?: string;
    /**
     * дом
     * @example "305"
     */
    house?: string;
    /**
     * Строение / Корпус
     */
    block?: string;
    /**
     * Квартира / Офис
     * @example "5"
     */
    flat?: string;
    /**
     * этаж
     * @example "2"
     */
    floor?: string;
    /**
     * подъезд
     */
    porch?: string;
    /**
     * код домофона
     * @example "001#777"
     */
    intercom?: string;
    /**
     * широта
     * @example "55.013548"
     */
    geo_lat?: string;
    /**
     * долгота
     * @example "36.115588"
     */
    geo_lon?: string;
}
