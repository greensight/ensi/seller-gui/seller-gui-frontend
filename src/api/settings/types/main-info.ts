import { CommonResponse } from '@api/common/types';
import { Address } from '@api/settings/types/address';

type Prettify<T> = {
    [K in keyof T]: T[K];
};

/**
 * Получение объектов типа Seller */

export interface SellerReadonlyProperties {
    /**
     * Идентификатор продавца
     * @example 1
     */
    id: number;
    /**
     * Статус продавца из SellerStatusEnum
     */
    status: number;
    /**
     * ID менеджера
     * @example 1
     */
    manager_id: number | null;
    /**
     * Время создания продавца
     * @example "2021-01-15T14:55:35.000000Z"
     */
    created_at: string;
    /**
     * Время обновления продавца
     * @example "2021-01-15T14:55:35.000000Z"
     */
    updated_at: string;
}

export interface SellerFillableProperties {
    /**
     * Юридическое наименование организации
     * @example "Сокольники"
     */
    legal_name: string;
    /**
     * Юридический адрес организации
     */
    legal_address?: Address | null;
    /**
     * ИНН
     */
    inn: string;
    /**
     * КПП
     */
    kpp?: string;
    /**
     * Номер расчетного счета
     */
    payment_account: string;
    /**
     * Номер корреспондентского счета банка
     */
    correspondent_account: string;
    /**
     * Наименование банка
     */
    bank: string;
    fact_address?: Address | null;
    bank_address: Address | null;
    /**
     * БИК банка
     */
    bank_bik: string;
    /**
     * Сайт компании
     */
    site: string;
    /**
     * Бренды и товары, которыми торгует продавец
     */
    info: string;
    /**
     * Статус продавца
     */
    status: number;
}

export type Seller = Prettify<SellerReadonlyProperties & SellerFillableProperties>;
export type SellerResponse = CommonResponse<Seller>;

/**
 * Частичное изменение объекта типа Seller */

export type SellerForPatch = Prettify<Partial<SellerReadonlyProperties & SellerFillableProperties>>;

export interface SellerStatusData {
    /**
     * ID статуса из UnitsSellerStatusEnum
     */
    id: number;
    /**
     * Название статуса
     */
    name: string;
}

/**
 * Получение объектов типа SellerStatus */
export interface GetSellerStatusesResponse {
    data: SellerStatusData[];
}
