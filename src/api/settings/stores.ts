import { useMutation, useQuery, useQueryClient } from '@tanstack/react-query';

import { CommonResponse, FetchError, Meta } from '@api/common/types';
import { useAuthApiClient } from '@api/hooks/useAuthApiClient';

import {
    StoreContactForCreate,
    StoreContactForPatch,
    StoreContactResponse,
    StorePickupTimeForCreate,
    StorePickupTimeForPatch,
    StorePickupTimeResponse,
    StoreWorkingForCreate,
    StoreWorkingForPatch,
    StoreWorkingResponse,
} from './types';
import {
    StoreForCreate,
    StoreForPatch,
    StoreForReplace,
    StoreResponse,
    StoresSearchRequest,
    StoresSearchResponse,
} from './types/stores';

const QueryKeys = {
    getStoresMeta: () => ['get-stores-meta'],
    searchStores: (data?: any) => (data ? ['search-stores', data] : ['search-stores']),
    getStore: (id?: number | string) => (id ? ['get-store', id] : ['get-store']),
};

/** Получение списка доступных полей для списка складов */
export function useStoresMeta(enabled = true) {
    const apiClient = useAuthApiClient();
    return useQuery<CommonResponse<Meta>, FetchError>({
        queryKey: QueryKeys.getStoresMeta(),
        queryFn: () => apiClient.get('units/stores:meta', {}),
        enabled,
    });
}

/** Получение объектов типа Store */
export function useSearchStores(data: StoresSearchRequest, enabled = true) {
    const apiClient = useAuthApiClient();
    return useQuery<StoresSearchResponse, FetchError>({
        queryKey: QueryKeys.searchStores(data),
        queryFn: () => apiClient.post('units/stores:search', { data }),
        enabled,
    });
}

/** Создание склада продавца */
export const useCreateStore = () => {
    const queryClient = useQueryClient();
    const apiClient = useAuthApiClient();
    return useMutation<StoreResponse, FetchError, StoreForCreate>(data => apiClient.post('units/stores', { data }), {
        onSuccess: () => {
            queryClient.invalidateQueries(QueryKeys.searchStores());
        },
    });
};

/** Получение объекта типа Store */
export function useStore({ id }: { id: number | string }, params: Record<string, any> = {}, enabled = true) {
    const apiClient = useAuthApiClient();
    return useQuery<StoreResponse, FetchError>({
        queryKey: QueryKeys.getStore(id),
        queryFn: () => apiClient.get(`units/stores/${id}`, { params }),
        enabled,
    });
}

/** Частичное изменение объекта типа Store */
export const usePatchStore = () => {
    const queryClient = useQueryClient();
    const apiClient = useAuthApiClient();
    return useMutation<StoreResponse, FetchError, { id: number | string } & StoreForPatch>(
        ({ id, ...data }) => apiClient.patch(`units/stores/${id}`, { data }),
        {
            onSuccess: ({ data }) => {
                queryClient.invalidateQueries(QueryKeys.searchStores());

                queryClient.invalidateQueries(QueryKeys.getStore(data?.id));
            },
        }
    );
};

/** Замена объекта типа Store */
export const useReplaceStore = () => {
    const queryClient = useQueryClient();
    const apiClient = useAuthApiClient();
    return useMutation<StoreResponse, FetchError, { id: number | string } & StoreForReplace>(
        ({ id, ...data }) => apiClient.put(`units/stores/${id}`, { data }),
        {
            onSuccess: ({ data }) => {
                queryClient.invalidateQueries(QueryKeys.searchStores());
                queryClient.invalidateQueries(QueryKeys.getStore(data?.id));
            },
        }
    );
};

/** Удаление объекта типа Store */
export const useDeleteStore = () => {
    const queryClient = useQueryClient();
    const apiClient = useAuthApiClient();
    return useMutation<any, FetchError, { id: number | string }>(
        ({ id }) => apiClient.delete(`units/stores/${id}`, {}),
        {
            onSuccess: () => {
                queryClient.invalidateQueries(QueryKeys.searchStores());
            },
        }
    );
};

/** Создание времени работы склада */
export const useCreateStoreWorking = () => {
    const queryClient = useQueryClient();
    const apiClient = useAuthApiClient();
    return useMutation<StoreWorkingResponse, FetchError, StoreWorkingForCreate>(
        data => apiClient.post('units/stores-workings', { data }),
        {
            onSuccess: data => {
                queryClient.invalidateQueries(QueryKeys.getStore(data.data.store_id));
            },
        }
    );
};

/** Частичное изменение объекта типа StoreWorking */
export const usePatchStoreWorking = () => {
    const queryClient = useQueryClient();
    const apiClient = useAuthApiClient();
    return useMutation<StoreWorkingResponse, FetchError, { id: number | string } & StoreWorkingForPatch>(
        ({ id, ...data }) => apiClient.patch(`units/stores-workings/${id}`, { data }),
        {
            onSuccess: () => {
                queryClient.invalidateQueries(QueryKeys.getStore());
            },
        }
    );
};

/** Удаление объекта типа StoreWorking */
export const useDeleteStoreWorking = () => {
    const apiClient = useAuthApiClient();
    const queryClient = useQueryClient();
    return useMutation<any, FetchError, { id: number | string }>(
        ({ id }) => apiClient.delete(`units/stores-workings/${id}`, {}),
        {
            onSuccess: () => {
                queryClient.invalidateQueries(QueryKeys.getStore());
            },
        }
    );
};

/** Создание времени отгрузки со склада */
export const useCreateStorePickupTime = () => {
    const apiClient = useAuthApiClient();
    const queryClient = useQueryClient();
    return useMutation<StorePickupTimeResponse, FetchError, StorePickupTimeForCreate>(
        data => apiClient.post('units/stores-pickup-times', { data }),
        {
            onSuccess: () => {
                queryClient.invalidateQueries(QueryKeys.getStore());
            },
        }
    );
};

/** Частичное изменение объекта типа StorePickupTime */
export const usePatchStorePickupTime = () => {
    const queryClient = useQueryClient();
    const apiClient = useAuthApiClient();
    return useMutation<StorePickupTimeResponse, FetchError, { id: number | string } & StorePickupTimeForPatch>(
        ({ id, ...data }) => apiClient.patch(`units/stores-pickup-times/${id}`, { data }),
        {
            onSuccess: () => {
                queryClient.invalidateQueries(QueryKeys.getStore());
            },
        }
    );
};

/** Удаление объекта типа StorePickupTime */
export const useDeleteStorePickupTime = () => {
    const queryClient = useQueryClient();
    const apiClient = useAuthApiClient();
    return useMutation<any, FetchError, { id: number | string }>(
        ({ id }) => apiClient.delete(`units/stores-pickup-times/${id}`, {}),
        {
            onSuccess: () => {
                queryClient.invalidateQueries(QueryKeys.getStore());
            },
        }
    );
};

/** Создание контактного лица склада */
export const useCreateStoreContact = () => {
    const queryClient = useQueryClient();
    const apiClient = useAuthApiClient();
    return useMutation<StoreContactResponse, FetchError, StoreContactForCreate>(
        data => apiClient.post('units/stores-contacts', { data }),
        {
            onSuccess: () => {
                queryClient.invalidateQueries(QueryKeys.getStore());
            },
        }
    );
};

/** Частичное изменение объекта типа StoreContact */
export const usePatchStoreContact = () => {
    const queryClient = useQueryClient();
    const apiClient = useAuthApiClient();
    return useMutation<StoreContactResponse, FetchError, { id: number | string } & StoreContactForPatch>(
        ({ id, ...data }) => apiClient.patch(`units/stores-contacts/${id}`, { data }),
        {
            onSuccess: () => {
                queryClient.invalidateQueries(QueryKeys.getStore());
            },
        }
    );
};

/** Удаление объекта типа StoreContact */
export const useDeleteStoreContact = () => {
    const queryClient = useQueryClient();
    const apiClient = useAuthApiClient();
    return useMutation<any, FetchError, { id: number | string }>(
        ({ id }) => apiClient.delete(`units/stores-contacts/${id}`, {}),
        {
            onSuccess: () => {
                queryClient.invalidateQueries(QueryKeys.getStore());
            },
        }
    );
};
