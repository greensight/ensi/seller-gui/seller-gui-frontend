import { CommonResponse, MetaEnumValue } from '@api/common/types';
import { useAuthApiClient } from '@api/hooks/useAuthApiClient';

export const useSellerAutocomplete = () => {
    const apiClient = useAuthApiClient();

    const asyncSearchFn = async (query: string) => {
        const res = (await apiClient.post(`units/seller-enum-values:search`, {
            data: {
                filter: {
                    query,
                },
            },
        })) as CommonResponse<MetaEnumValue[]>;
        return {
            options: res.data.map(e => ({
                key: e.title,
                value: e.id,
            })),
            hasMore: false,
        };
    };

    const asyncOptionsByValuesFn = async (vals: number[], abortController: AbortController) => {
        const actualValues = vals.map(v => {
            if (typeof v === 'object' && 'value' in v) return Number((v as any).value);
            return Number(v);
        });

        const res = (await apiClient.post(`units/seller-enum-values:search`, {
            data: {
                filter: { id: actualValues },
            },
            abortController,
        })) as CommonResponse<MetaEnumValue[]>;

        return res.data.map(e => ({
            key: e.title,
            value: e.id,
        }));
    };

    return {
        asyncOptionsByValuesFn,
        asyncSearchFn,
    };
};
