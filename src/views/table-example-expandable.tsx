/* eslint-disable react/no-unstable-nested-components */
import {
    ColumnDef,
    ExpandedState,
    Row,
    createColumnHelper,
    getCoreRowModel,
    getExpandedRowModel,
    getFilteredRowModel,
    getPaginationRowModel,
} from '@tanstack/react-table';
import { endOfYesterday, startOfDay } from 'date-fns';
import { MouseEvent, useMemo, useState } from 'react';

import PageWrapper from '@components/PageWrapper';
import Table, { TooltipItem, useTable } from '@components/Table';
import { getSelectColumn, getSettingsColumn } from '@components/Table/columns';
import { Cell } from '@components/Table/components';

import { Button, colors, scale } from '@scripts/gds';
import { getNumberId } from '@scripts/mock';

import ChevronDownIcon from '@icons/small/chevronDown.svg';

const categories = [
    {
        id: getNumberId(),
        title: 'Мужчины',
        code: 'Man',
        created_at: endOfYesterday(),
        updated_at: new Date(),
        status: 'Активный',
        subRows: [
            {
                id: getNumberId(),
                title: 'Штаны',
                code: 'shtani',
                created_at: endOfYesterday(),
                updated_at: new Date(),
                status: 'Активный',
                subRows: [
                    {
                        id: getNumberId(),
                        title: 'Спортивные',
                        code: 'Sport',
                        created_at: endOfYesterday(),
                        updated_at: new Date(),
                        status: 'Активный',
                    },
                    {
                        id: getNumberId(),
                        title: 'Брюки',
                        code: 'Bruki',
                        created_at: endOfYesterday(),
                        updated_at: new Date(),
                        status: 'Активный',
                    },
                    {
                        id: getNumberId(),
                        title: 'Джоггеры',
                        code: 'Joggeri',
                        created_at: endOfYesterday(),
                        updated_at: new Date(),
                        status: 'Активный',
                    },
                    {
                        id: getNumberId(),
                        title: 'Джинсы',
                        code: 'Joggeri',
                        created_at: startOfDay(new Date()),
                        updated_at: new Date(),
                        status: 'Активный',
                    },
                ],
            },
            {
                id: getNumberId(),
                title: 'Футболки',
                code: 'Footbolki',
                created_at: endOfYesterday(),
                updated_at: new Date(),
                status: 'Активный',
                subRows: [
                    {
                        id: getNumberId(),
                        title: 'Безрукавки',
                        code: 'Bezrukavki',
                        created_at: endOfYesterday(),
                        updated_at: new Date(),
                        status: 'Активный',
                    },
                    {
                        id: getNumberId(),
                        title: 'Поло',
                        code: 'Polo',
                        created_at: endOfYesterday(),
                        updated_at: new Date(),
                        status: 'Активный',
                    },
                ],
            },
        ],
    },
    {
        id: getNumberId(),
        title: 'Женщины',
        code: 'Woman',
        created_at: endOfYesterday(),
        updated_at: new Date(),
        status: 'Активный',
        subRows: [
            {
                id: getNumberId(),
                title: 'Штаны',
                code: 'shtani',
                created_at: endOfYesterday(),
                updated_at: new Date(),
                status: 'Активный',
                subRows: [
                    {
                        id: getNumberId(),
                        title: 'Спортивные',
                        code: 'Sport',
                        created_at: endOfYesterday(),
                        updated_at: new Date(),
                        status: 'Активный',
                    },
                    {
                        id: getNumberId(),
                        title: 'Брюки',
                        code: 'Bruki',
                        created_at: endOfYesterday(),
                        updated_at: new Date(),
                        status: 'Активный',
                    },
                    {
                        id: getNumberId(),
                        title: 'Джоггеры',
                        code: 'Joggeri',
                        created_at: endOfYesterday(),
                        updated_at: new Date(),
                        status: 'Активный',
                    },
                    {
                        id: getNumberId(),
                        title: 'Джинсы',
                        code: 'Joggeri',
                        created_at: startOfDay(new Date()),
                        updated_at: new Date(),
                        status: 'Активный',
                    },
                ],
            },
            {
                id: getNumberId(),
                title: 'Футболки',
                code: 'Footbolki',
                created_at: endOfYesterday(),
                updated_at: new Date(),
                status: 'Активный',
                subRows: [
                    {
                        id: getNumberId(),
                        title: 'Безрукавки',
                        code: 'Bezrukavki',
                        created_at: endOfYesterday(),
                        updated_at: new Date(),
                        status: 'Активный',
                    },
                    {
                        id: getNumberId(),
                        title: 'Поло',
                        code: 'Polo',
                        created_at: endOfYesterday(),
                        updated_at: new Date(),
                        status: 'Активный',
                    },
                ],
            },
        ],
    },
];

const tooltipAction = (rows?: Row<any> | Row<any>[]) => {
    if (Array.isArray(rows)) {
        alert(`You want to make multi action with ${rows.length} selected row(s)`);
    } else if (rows) {
        alert(`You want to make action with 1 row which id is ${rows.id}`);
    } else {
        alert(`You want to make action without selecting strings`);
    }
};

export default function Home() {
    const tooltipContent: TooltipItem[] = useMemo(
        () => [
            {
                type: 'edit',
                text: 'Изменить статус',
                action: tooltipAction,
            },
            {
                type: 'edit',
                text: 'Изменить атрибут',
                action: tooltipAction,
            },
            {
                type: 'edit',
                text: 'Изменить категоризацию',
                action: tooltipAction,
            },
            {
                type: 'edit',
                text: 'Задать признаки',
                action: tooltipAction,
            },
            {
                type: 'edit',
                text: 'Отправить на модерацию',
                action: tooltipAction,
            },
            {
                type: 'copy',
                text: 'Копировать ID',
                action: tooltipAction,
            },
            {
                type: 'copy',
                text: 'Копировать артикул',
                action: tooltipAction,
            },
            {
                type: 'export',
                text: 'Экспорт товаров',
                action: tooltipAction,
            },
            {
                type: 'delete',
                text: 'Удалить',
                action: tooltipAction,
            },
        ],
        []
    );

    const [expanded, setExpanded] = useState<ExpandedState>({ 0: true });

    const columnHelper = createColumnHelper<typeof categories>();

    const columns: ColumnDef<typeof categories>[] = useMemo(
        () => [
            getSelectColumn<typeof categories>(undefined, 'Example'),
            columnHelper.accessor('id', {
                header: 'ID',
                cell: props => props.getValue(),
            }),
            columnHelper.accessor('title', {
                header: 'Названиe',
                cell: ({ getValue, row }) => (
                    <p css={{ paddingLeft: (row?.depth || 0) * scale(4) + (row?.getCanExpand() ? 0 : scale(4)) }}>
                        {row?.getCanExpand() && row.toggleExpanded && (
                            <Button
                                theme="ghost"
                                Icon={ChevronDownIcon}
                                onClick={(e: MouseEvent<HTMLButtonElement>) => {
                                    if (row.toggleExpanded) {
                                        e.stopPropagation();
                                        row.toggleExpanded();
                                    }
                                }}
                                hidden
                                css={{
                                    ':hover': {
                                        background: 'transparent !important',
                                    },
                                    svg: {
                                        transition: 'transform 0.2s ease',
                                        ...(row?.getIsExpanded() && { transform: 'rotate(-180deg)' }),
                                    },
                                }}
                            >
                                Развернуть/свернуть ряд
                            </Button>
                        )}
                        {getValue() as any}
                    </p>
                ),
            }),
            columnHelper.accessor('code', {
                header: 'Названиe',
                cell: props => props.getValue(),
            }),
            columnHelper.accessor('created_at', {
                header: 'Создано',
                cell: ({ getValue }) => <Cell value={getValue()} type="datetime" />,
            }),
            columnHelper.accessor('updated_at', {
                header: 'Обновлено',
                cell: ({ getValue }) => <Cell value={getValue()} type="datetime" />,
            }),
            columnHelper.accessor('status', {
                header: 'Статус',
                cell: ({ getValue }) => (
                    <p>
                        <span
                            css={{
                                width: 6,
                                height: 6,
                                background: colors.success,
                                borderRadius: '50%',
                                display: 'inline-block',
                                verticalAlign: 'middle',
                                marginRight: scale(1),
                            }}
                        />

                        {getValue() as any}
                    </p>
                ),
            }),
            getSettingsColumn({ tooltipContent }),
        ],
        []
    );

    const table = useTable(
        {
            data: categories as any,
            columns,
            meta: {
                tableKey: `store_pickup_times`,
            },
            state: {
                expanded,
            },
            onExpandedChange: setExpanded,
            getCoreRowModel: getCoreRowModel(),
            getPaginationRowModel: getPaginationRowModel(),
            getFilteredRowModel: getFilteredRowModel(),
            getExpandedRowModel: getExpandedRowModel(),
            // debugTable: true,
        },
        []
    );

    return (
        <PageWrapper title="Пример страницы с разворачивающейся таблицей" css={{ padding: 0 }}>
            <Table instance={table} />
        </PageWrapper>
    );
}
