import Link from 'next/link';
import { useRouter } from 'next/router';
import { useCallback, useMemo } from 'react';

import { useSearchStores, useStoresMeta } from '@api/settings';
import { Store } from '@api/settings/types';

import ListBuilder from '@components/ListBuilder';
import { ExtendedRow, TooltipItem } from '@components/Table';

import { Button } from '@scripts/gds';

import PlusIcon from '@icons/plus.svg';

const StoresList = () => {
    const { push, pathname } = useRouter();

    const goToDetailPage = useCallback(
        (originalRows: ExtendedRow['original'] | ExtendedRow['original'][] | undefined) => {
            if (!Array.isArray(originalRows)) push(`${pathname}/${originalRows?.original.id}`);
        },
        [pathname, push]
    );

    const tooltipContent = useMemo<TooltipItem[]>(
        () => [
            {
                type: 'edit',
                text: 'Редактировать склад',
                action: goToDetailPage,
                isDisable: () => false,
            },
        ],
        [goToDetailPage]
    );
    const headerInner = () => (
        <Link href={`${pathname}/create`} passHref>
            <Button Icon={PlusIcon} as="a">
                Создать склад
            </Button>
        </Link>
    );

    return (
        <ListBuilder<Store>
            searchHook={useSearchStores}
            metaHook={useStoresMeta}
            tooltipItems={tooltipContent}
            headerInner={headerInner}
            title="Список складов"
        />
    );
};

export default StoresList;
