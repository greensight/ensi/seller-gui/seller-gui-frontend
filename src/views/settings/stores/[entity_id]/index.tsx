import { useRouter } from 'next/router';
import { useMemo, useRef } from 'react';
import * as Yup from 'yup';

import { useCreateStore, useDeleteStore, usePatchStore, useStore } from '@api/settings';
import { StoreAddress, StoreForCreate, StoreForPatch, StoresSearchRequestInclude } from '@api/settings/types';

import { useSuccess } from '@context/modal';

import CopyButton from '@components/CopyButton';
import FormWrapper from '@components/FormWrapper';
import InfoList from '@components/InfoList';
import { getDefaultDates } from '@components/InfoList/helper';
import PageControls from '@components/PageControls';
import { PageTemplate } from '@components/PageTemplate';
import PageWrapper from '@components/PageWrapper';
import Switcher from '@components/controls/Switcher';
import Tabs from '@components/controls/Tabs';
import Form from '@components/controls/future/Form';

import { useActionPopup } from '@hooks/useActionPopup';

import { CREATE_PARAM, ErrorMessages, ModalMessages } from '@scripts/constants';
import { ButtonNameEnum, RedirectMethods } from '@scripts/enums';
import { scale } from '@scripts/gds';
import { pickChangedFormValues } from '@scripts/helpers';
import { usePopupState } from '@scripts/hooks';
import { useFutureTabs } from '@scripts/hooks/useFutureTabs';

import { EditContactPopup, EditContactPopupState } from './components/EditContactPopup';
import { EditPickupTimePopup, EditPickupTimePopupState } from './components/EditPickupTimePopup';
import { EditWorkingPopup, EditWorkingPopupState } from './components/EditWorkingPopup';
import { Contacts } from './tabs/Contacts';
import { MainData } from './tabs/MainData';
import { PickupTimes } from './tabs/PickupTimes';
import { Workings } from './tabs/Workings';

const Store = () => {
    const { query, pathname, push } = useRouter();
    const listLink = pathname.split(`[entity_id]`)[0];

    const { getTabsProps } = useFutureTabs();

    const id = +(query.entity_id?.toString() || '');
    const isCreationPage = query.entity_id === CREATE_PARAM;

    const { data: apiStore, isFetching: isLoading } = useStore(
        { id },
        {
            include: Object.values(StoresSearchRequestInclude),
        },
        !Number.isNaN(+id)
    );
    const store = apiStore?.data;

    const createStore = useCreateStore();
    const patchStore = usePatchStore();
    const deleteStore = useDeleteStore();

    const { popupState, popupDispatch, ActionPopup, ActionEnum, ActionType } = useActionPopup();

    const [workingState, workingDispatch] = usePopupState<Partial<EditWorkingPopupState>>({ open: false, storeId: id });
    const [contactState, contactDispatch] = usePopupState<Partial<EditContactPopupState>>({ open: false, storeId: id });
    const [pickupState, pickupDispatch] = usePopupState<Partial<EditPickupTimePopupState>>({
        open: false,
        storeId: id,
    });

    useSuccess(createStore.isSuccess ? ModalMessages.SUCCESS_CREATE : '');
    useSuccess(patchStore.isSuccess ? ModalMessages.SUCCESS_UPDATE : '');

    const buttonNameRef = useRef<ButtonNameEnum | null>(null);

    const pageTitle = isCreationPage ? 'Создание склада' : `Редактирование склада #${id}`;

    const initialValues = useMemo(
        () => ({
            name: store?.name || '',
            active: store?.active || false,
            address_string: store?.address.address_string || '',
            postal_index: store?.address.post_index || '',
            timezone: store?.timezone || '',
        }),
        [store]
    );

    return (
        <>
            <PageWrapper title={pageTitle} isLoading={isLoading || createStore.isLoading || patchStore.isLoading}>
                <FormWrapper
                    initialValues={initialValues}
                    enableReinitialize
                    validationSchema={Yup.object().shape({
                        name: Yup.string().required(ErrorMessages.REQUIRED),
                        seller_id: Yup.number()
                            .transform(val => (Number.isNaN(val) ? undefined : val))
                            .optional(),
                        timezone: Yup.string().nullable(),
                        address_string: Yup.string().optional().nullable(),
                    })}
                    css={{ width: '100%', marginBottom: scale(3) }}
                    onSubmit={async values => {
                        const addressObj: StoreAddress = {
                            address_string: values.address_string,
                            post_index: values.postal_index,
                            country_code: '-',
                            region: '-',
                            region_guid: '-',
                            city: '-',
                            city_guid: '-',
                            geo_lat: '-',
                            geo_lon: '-',
                        };

                        if (isCreationPage) {
                            const createRequest: StoreForCreate = {
                                active: values.active || false,
                                name: values.name || '',
                                address: addressObj,
                                timezone: values.timezone,
                            };

                            const response = await createStore.mutateAsync(createRequest);

                            if (buttonNameRef.current === ButtonNameEnum.APPLY) {
                                workingDispatch({
                                    type: ActionType.PreClose,
                                    payload: { storeId: response.data.id },
                                });
                                contactDispatch({
                                    type: ActionType.PreClose,
                                    payload: { storeId: response.data.id },
                                });
                                pickupDispatch({
                                    type: ActionType.PreClose,
                                    payload: { storeId: response.data.id },
                                });

                                return {
                                    method: RedirectMethods.replace,
                                    redirectPath: `/settings/stores/${response.data.id}`,
                                };
                            }

                            return {
                                method: RedirectMethods.push,
                                redirectPath: '/settings/stores',
                            };
                        }

                        const initialValuesPicked: StoreForPatch = {
                            name: initialValues.name,
                            active: initialValues.active,

                            address: {
                                address_string: initialValues.address_string,
                                post_index: initialValues.postal_index,
                                country_code: '-',
                                region: '-',
                                region_guid: '-',
                                city: '-',
                                city_guid: '-',
                                geo_lat: '-',
                                geo_lon: '-',
                            },
                            timezone: initialValues.timezone,
                        };

                        const newPatchData: StoreForPatch = {
                            active: values.active || false,
                            name: values.name || '',
                            address: addressObj,
                            timezone: values.timezone === null ? '' : values.timezone,
                        };

                        const patchRequest = pickChangedFormValues(initialValuesPicked, newPatchData);
                        await patchStore.mutateAsync({ id, ...patchRequest });

                        if (buttonNameRef.current === ButtonNameEnum.APPLY) {
                            return {
                                method: RedirectMethods.replace,
                                redirectPath: `/settings/stores/${id}`,
                            };
                        }

                        return {
                            method: RedirectMethods.push,
                            redirectPath: '/settings/stores',
                        };
                    }}
                >
                    {({ reset }) => (
                        <PageTemplate
                            h1={pageTitle}
                            backlink={{ text: 'К списку складов', href: '/settings/stores' }}
                            customChildren
                            controls={
                                <PageControls>
                                    <PageControls.Delete
                                        onClick={() => {
                                            popupDispatch({
                                                type: ActionType.Delete,
                                                payload: {
                                                    title: `Вы уверены, что хотите удалить склад №${id}?`,
                                                    popupAction: ActionEnum.DELETE,
                                                    onAction: async () => {
                                                        if (!id) return;
                                                        reset();
                                                        await deleteStore.mutateAsync({ id });

                                                        push({ pathname: listLink });
                                                    },
                                                },
                                            });
                                        }}
                                    />
                                    <PageControls.Close onClick={() => push(listLink)} />
                                    <PageControls.Apply
                                        onClick={() => {
                                            buttonNameRef.current = ButtonNameEnum.APPLY;
                                        }}
                                    />
                                    <PageControls.Save
                                        onClick={() => {
                                            buttonNameRef.current = ButtonNameEnum.SAVE;
                                        }}
                                    />
                                </PageControls>
                            }
                            aside={
                                <InfoList>
                                    <InfoList.Item
                                        value={
                                            <Form.Field name="active" label="Активность">
                                                <Switcher>Активность</Switcher>
                                            </Form.Field>
                                        }
                                    />
                                    <InfoList.Item
                                        name="ID:"
                                        value={!isCreationPage ? <CopyButton>{`${id}`}</CopyButton> : '-'}
                                    />
                                    {getDefaultDates(store || {}).map(item => (
                                        <InfoList.Item key={item.name} {...item} />
                                    ))}
                                </InfoList>
                            }
                        >
                            <div css={{ display: 'flex', width: '100%' }}>
                                <Tabs {...getTabsProps()} css={{ width: '100%' }}>
                                    <Tabs.Tab title="Основные данные" id="0">
                                        <MainData />
                                    </Tabs.Tab>
                                    <Tabs.Tab title="Время работы" id="1" hidden={isCreationPage}>
                                        <Workings store={store} dispatch={workingDispatch} />
                                    </Tabs.Tab>
                                    <Tabs.Tab title="Контакты" id="2" hidden={isCreationPage}>
                                        <Contacts store={store} dispatch={contactDispatch} />
                                    </Tabs.Tab>
                                    <Tabs.Tab title="График отгрузки" id="3" hidden={isCreationPage}>
                                        <PickupTimes store={store} dispatch={pickupDispatch} />
                                    </Tabs.Tab>
                                </Tabs>
                            </div>
                        </PageTemplate>
                    )}
                </FormWrapper>
            </PageWrapper>
            <EditWorkingPopup state={workingState} dispatch={workingDispatch} />
            <EditContactPopup state={contactState} dispatch={contactDispatch} />
            <EditPickupTimePopup state={pickupState} dispatch={pickupDispatch} />
            <ActionPopup popupState={popupState} popupDispatch={popupDispatch} />
        </>
    );
};

export default Store;
