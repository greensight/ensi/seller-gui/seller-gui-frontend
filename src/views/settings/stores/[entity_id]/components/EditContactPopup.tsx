import { Dispatch, useCallback, useMemo } from 'react';
import * as Yup from 'yup';

import { useCreateStoreContact, usePatchStoreContact } from '@api/settings';

import Mask from '@components/controls/Mask';
import Form from '@components/controls/future/Form';
import Popup from '@components/controls/future/Popup';

import { ErrorMessages } from '@scripts/constants';
import { ActionType } from '@scripts/enums';
import { Button, Layout } from '@scripts/gds';
import { Action } from '@scripts/hooks/usePopupState';
import { maskPhone } from '@scripts/mask';

type ContactData = {
    id: number;
    name: string;
    phone: string;
    email: string;
};

export type EditContactPopupState = {
    data?: ContactData | null;
    storeId: number;
    open: boolean;
    action: ActionType;
};

export interface EditContactPopupProps {
    state: Partial<EditContactPopupState>;
    dispatch: Dispatch<Action<Partial<EditContactPopupState>>>;
}

export const EditContactPopup = ({ state, dispatch }: EditContactPopupProps) => {
    const { data, storeId, action } = state;
    const isCreation = action === ActionType.Add;

    const initialValues = useMemo<ContactData>(
        () => ({
            id: data?.id!,
            email: data?.email || '',
            name: data?.name || '',
            phone: data?.phone || '',
        }),
        [data]
    );

    const createContact = useCreateStoreContact();
    const updateContact = usePatchStoreContact();

    const onSubmit = useCallback(
        async (values: any) => {
            if (isCreation) {
                await createContact.mutateAsync({
                    email: values.email,
                    name: values.name,
                    phone: values.phone,
                    store_id: storeId!,
                });

                dispatch({ type: ActionType.Close });
                return;
            }

            await updateContact.mutateAsync({
                ...values,
                id: data?.id,
                store_id: storeId!,
            });

            dispatch({ type: ActionType.Close });
        },
        [createContact, data?.id, dispatch, isCreation, storeId, updateContact]
    );

    const isLoading = createContact.isLoading || updateContact.isLoading;

    return (
        <Popup
            open={state.open!}
            onClose={() => dispatch({ type: ActionType.Close })}
            size="minMd"
            isLoading={isLoading}
        >
            <Form
                initialValues={initialValues}
                onSubmit={onSubmit}
                validationSchema={Yup.object({
                    name: Yup.string().required(ErrorMessages.REQUIRED),
                    phone: Yup.string().required(ErrorMessages.REQUIRED).min(17, ErrorMessages.REQUIRED),
                    email: Yup.string().email(ErrorMessages.EMAIL).required(ErrorMessages.REQUIRED),
                })}
            >
                <Popup.Header title={`${isCreation ? 'Создание' : 'Редактирование'} контактного лица`} />
                <Popup.Content>
                    <Layout cols={1}>
                        <Form.Field name="name" label="ФИО" />
                        <Form.Field name="phone" label="Телефон" type="tel">
                            <Mask mask={maskPhone} />
                        </Form.Field>
                        <Form.Field name="email" label="Email" />
                    </Layout>
                </Popup.Content>
                <Popup.Footer>
                    <Button theme="outline" onClick={() => dispatch({ type: ActionType.Close })}>
                        Отменить
                    </Button>
                    <Button type="submit">Сохранить</Button>
                </Popup.Footer>
            </Form>
        </Popup>
    );
};
