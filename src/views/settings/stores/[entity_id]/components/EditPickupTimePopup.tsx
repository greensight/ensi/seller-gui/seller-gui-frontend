import { Dispatch, useCallback, useMemo } from 'react';
import * as Yup from 'yup';

import { useGetDeliveryServicesAutocomplete } from '@api/logistic';
import { useCreateStorePickupTime, usePatchStorePickupTime } from '@api/settings';

import Mask from '@controls/Mask';
import AutocompleteAsync from '@controls/future/AutocompleteAsync';
import Form from '@controls/future/Form';
import Popup from '@controls/future/Popup';
import Select from '@controls/future/Select';

import { ErrorMessages } from '@scripts/constants';
import { ActionType, daysValues } from '@scripts/enums';
import { Button, Layout } from '@scripts/gds';
import { prepareEnumForFutureSelect } from '@scripts/helpers';
import { Action } from '@scripts/hooks/usePopupState';
import { TIME_WITHOUT_SECONDS_MASK } from '@scripts/mask';

type PickupTimeData = {
    id: number;
    day: number | '';
    pickup_time_start?: string;
    pickup_time_end?: string;
    delivery_service?: number | null;
    pickup_time_code?: string;
};

export type EditPickupTimePopupState = {
    data?: PickupTimeData | null;
    storeId: number;
    open: boolean;
    action: ActionType;
};

export interface EditPickupTimePopupProps {
    state: Partial<EditPickupTimePopupState>;
    dispatch: Dispatch<Action<Partial<EditPickupTimePopupState>>>;
}

const dayOptions = prepareEnumForFutureSelect(daysValues).map(e => ({ key: e.key, value: +e.value }));

export const EditPickupTimePopup = ({ state, dispatch }: EditPickupTimePopupProps) => {
    const { data, storeId, action } = state;
    const isCreation = action === ActionType.Add;

    const initialValues = useMemo<Omit<PickupTimeData, 'id'>>(
        () => ({
            day: data?.day || '',
            delivery_service: data?.delivery_service || null,
            pickup_time_start: data?.pickup_time_start || '',
            pickup_time_end: data?.pickup_time_end || '',
        }),
        [data]
    );

    const createPickupTime = useCreateStorePickupTime();
    const updatePickupTime = usePatchStorePickupTime();

    const { deliveryServicesOptionsByValuesFn, deliveryServicesSearchFn } = useGetDeliveryServicesAutocomplete();

    const onSubmit = useCallback(
        async (values: { [K in keyof PickupTimeData]-?: PickupTimeData[K] }) => {
            if (isCreation) {
                await createPickupTime.mutateAsync({
                    day: +values.day,
                    delivery_service: values.delivery_service,
                    store_id: storeId!,
                    pickup_time_end: values.pickup_time_end,
                    pickup_time_start: values.pickup_time_start,
                });

                dispatch({ type: ActionType.Close });
                return;
            }

            await updatePickupTime.mutateAsync({
                id: data?.id!,
                day: +values.day,
                delivery_service: values.delivery_service,
                pickup_time_end: values.pickup_time_end,
                pickup_time_start: values.pickup_time_start,
            });

            dispatch({ type: ActionType.Close });
        },
        [createPickupTime, data?.id, dispatch, isCreation, storeId, updatePickupTime]
    );

    const isLoading = createPickupTime.isLoading || updatePickupTime.isLoading;

    return (
        <Popup
            open={state.open!}
            onClose={() => dispatch({ type: ActionType.Close })}
            size="minMd"
            isLoading={isLoading}
        >
            <Form
                initialValues={initialValues}
                onSubmit={onSubmit}
                validationSchema={Yup.object({
                    day: Yup.number()
                        .transform(val => (Number.isNaN(+val) ? undefined : val))
                        .required(ErrorMessages.REQUIRED),
                    pickup_time_start: Yup.string()
                        .test(`time-format`, 'Неверный формат времени', val => val?.length === 5)
                        .required(ErrorMessages.REQUIRED),

                    pickup_time_end: Yup.string()
                        .test(`time-format`, 'Неверный формат времени', val => val?.length === 5)
                        .required(ErrorMessages.REQUIRED),
                    delivery_service: Yup.number()
                        .nullable()
                        .transform(val => (Number.isNaN(+val) ? undefined : val)),
                })}
            >
                <Popup.Header title={`${isCreation ? 'Создание' : 'Редактирование'} времени отгрузки`} />
                <Popup.Content>
                    <Layout cols={1}>
                        <Layout cols={1}>
                            <Form.Field name="day" label="День недели">
                                <Select options={dayOptions} hideClearButton />
                            </Form.Field>
                            <Form.Field name="delivery_service" label="Служба доставки">
                                <AutocompleteAsync
                                    asyncOptionsByValuesFn={deliveryServicesOptionsByValuesFn}
                                    asyncSearchFn={deliveryServicesSearchFn}
                                />
                            </Form.Field>
                            <Form.Field name="pickup_time_start" label="Время начала отгрузки" placeholder="00:00">
                                <Mask {...TIME_WITHOUT_SECONDS_MASK} />
                            </Form.Field>
                            <Form.Field name="pickup_time_end" label="Время окончания отгрузки" placeholder="00:00">
                                <Mask {...TIME_WITHOUT_SECONDS_MASK} />
                            </Form.Field>
                        </Layout>
                    </Layout>
                </Popup.Content>
                <Popup.Footer>
                    <Button theme="outline" onClick={() => dispatch({ type: ActionType.Close })}>
                        Отменить
                    </Button>
                    <Button type="submit">Сохранить</Button>
                </Popup.Footer>
            </Form>
        </Popup>
    );
};
