import { Dispatch, useCallback, useMemo } from 'react';
import * as Yup from 'yup';

import Legend from '@controls/Legend';
import Loader from '@controls/Loader';
import Mask from '@controls/Mask';
import Switcher from '@controls/Switcher';
import Form from '@controls/future/Form';
import Popup from '@controls/future/Popup';
import Select from '@controls/future/Select';

import { ErrorMessages } from '@scripts/constants';
import { ActionType, Days, daysValues } from '@scripts/enums';
import { Button, Layout } from '@scripts/gds';
import { prepareEnumForFutureSelect } from '@scripts/helpers';
import { Action } from '@scripts/hooks/usePopupState';
import { TIME_WITHOUT_SECONDS_MASK } from '@scripts/mask';
import { useCreateStoreWorking, usePatchStoreWorking } from '@api/settings';

type WorkingData = {
    id: number;
    active: boolean;
    day: number | '';
    working_start_time?: string | null;
    working_end_time?: string | null;
};

export type EditWorkingPopupState = {
    data?: WorkingData | null;
    storeId: number;
    open: boolean;
    action: ActionType;
};

const dayOptions = prepareEnumForFutureSelect(daysValues).map(e => ({ key: e.key, value: +e.value }));

export interface EditWorkingPopupProps {
    state: Partial<EditWorkingPopupState>;
    dispatch: Dispatch<Action<Partial<EditWorkingPopupState>>>;
}

export const EditWorkingPopup = ({ state, dispatch }: EditWorkingPopupProps) => {
    const { data, storeId, action } = state;
    const isCreation = action === ActionType.Add;

    const initialValues = useMemo<WorkingData>(
        () => ({
            id: data?.id!,
            active: data?.active || false,
            day: data?.day ? +`${data?.day}` : Days.MONDAY,
            working_end_time: data?.working_end_time || '',
            working_start_time: data?.working_start_time || '',
        }),
        [data]
    );

    const createWorking = useCreateStoreWorking();
    const updateWorking = usePatchStoreWorking();

    const onSubmit = useCallback(
        async (values: any) => {
            if (isCreation) {
                await createWorking.mutateAsync({
                    ...values,
                    day: +values.day,
                    store_id: storeId,
                });

                dispatch({ type: ActionType.Close });
                return;
            }

            await updateWorking.mutateAsync({
                ...values,
                id: data?.id,
                day: +values.day,
            });

            dispatch({ type: ActionType.Close });
        },
        [createWorking, data?.id, dispatch, isCreation, storeId, updateWorking]
    );

    const isLoading = createWorking.isLoading || updateWorking.isLoading;

    return (
        <Popup open={state.open!} onClose={() => dispatch({ type: ActionType.Close })} size="minMd">
            <Form
                initialValues={initialValues}
                onSubmit={onSubmit}
                validationSchema={Yup.object({
                    active: Yup.boolean(),
                    day: Yup.number()
                        .transform(val => (Number.isNaN(+val) ? undefined : val))
                        .required(ErrorMessages.REQUIRED),
                })}
            >
                {isLoading && <Loader />}
                <Popup.Header title={`${isCreation ? 'Создание' : 'Редактирование'} времени работы`} />
                <Popup.Content>
                    <Layout cols={1}>
                        <Form.Field name="active" label="Активность">
                            <Legend label="Активность" />
                            <Switcher css={{ width: 'fit-content' }}>Да</Switcher>
                        </Form.Field>
                        <Form.Field name="day" label="День недели">
                            <Select options={dayOptions} hideClearButton />
                        </Form.Field>
                        <Form.Field name="working_start_time" label="Время начала работы склада" placeholder="00:00">
                            <Mask {...TIME_WITHOUT_SECONDS_MASK} />
                        </Form.Field>
                        <Form.Field name="working_end_time" label="Время конца работы склада" placeholder="00:00">
                            <Mask {...TIME_WITHOUT_SECONDS_MASK} />
                        </Form.Field>
                    </Layout>
                </Popup.Content>
                <Popup.Footer>
                    <Button theme="outline" onClick={() => dispatch({ type: ActionType.Close })}>
                        Отменить
                    </Button>
                    <Button type="submit">Сохранить</Button>
                </Popup.Footer>
            </Form>
        </Popup>
    );
};
