import Autocomplete from '@controls/future/Autocomplete';
import Form from '@controls/future/Form';

import Block from '@components/Block';

import { Layout } from '@scripts/gds';
import { useTimezones } from '@scripts/hooks';

export const MainData = () => {
    const timezoneOptions = useTimezones().map(e => ({ key: e.label, value: e.value }));

    return (
        <Block css={{ width: '100%', borderTopLeftRadius: 0, borderTopRightRadius: 0 }}>
            <Block.Body>
                <Layout cols={1}>
                    <Layout.Item>
                        <Form.Field name="name" label="Название склада" />
                    </Layout.Item>

                    <Layout.Item>
                        <Form.Field label="Адрес" name="address_string" />
                    </Layout.Item>
                    <Layout.Item>
                        <Form.Field label="Почтовый индекс" name="postal_index" />
                    </Layout.Item>
                    <Layout.Item>
                        <Form.Field label="Часовой пояс склада" name="timezone">
                            <Autocomplete options={timezoneOptions} />
                        </Form.Field>
                    </Layout.Item>
                </Layout>
            </Block.Body>
        </Block>
    );
};
