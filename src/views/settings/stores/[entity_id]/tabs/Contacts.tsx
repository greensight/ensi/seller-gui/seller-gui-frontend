import { createColumnHelper, CellContext } from '@tanstack/react-table';
import { useMemo, useRef, useCallback } from 'react';

import { ActionEnum } from '@components/ActionPopup';
import Block from '@components/Block';
import LoadWrapper from '@components/controls/LoadWrapper';
import Table, { useSorting, useTable } from '@components/Table';

import { ActionType } from '@scripts/enums';
import { Button, Layout, scale } from '@scripts/gds';
import { formatDate } from '@scripts/helpers';
import { useActionPopup } from '@scripts/hooks/useActionPopup';

import EditIcon from '@icons/small/edit.svg';
import AddIcon from '@icons/small/plus.svg';
import DeleteIcon from '@icons/small/trash.svg';

import { Store, StoreContact } from '@api/settings/types';
import { useDeleteStoreContact } from '@api/settings';
import { EditContactPopupProps } from '../components/EditContactPopup';

const columnHelper = createColumnHelper<StoreContact>();

export const Contacts = ({ store, dispatch }: { store?: Store; dispatch: EditContactPopupProps['dispatch'] }) => {
    const data = useMemo(() => store?.contacts || [], [store?.contacts]);

    const deleteContact = useDeleteStoreContact();

    const deleteContactAsync = useRef(deleteContact.mutateAsync);
    deleteContactAsync.current = deleteContact.mutateAsync;

    const { popupState, popupDispatch, ActionPopup } = useActionPopup();

    const SettingsCell = useCallback(
        ({ row: { original } }: CellContext<StoreContact, unknown>) => (
            <Layout type="flex">
                <Layout.Item>
                    <Button
                        hidden
                        Icon={EditIcon}
                        onClick={() => {
                            dispatch({
                                type: ActionType.Edit,
                                payload: {
                                    data: {
                                        id: original.id,
                                        email: original.email,
                                        name: original.name,
                                        phone: original.phone,
                                    },
                                    open: true,
                                    storeId: store?.id!,
                                },
                            });
                        }}
                    >
                        изменить
                    </Button>
                </Layout.Item>
                <Layout.Item>
                    <Button
                        hidden
                        Icon={DeleteIcon}
                        theme="dangerous"
                        onClick={() => {
                            popupDispatch({
                                type: ActionType.Delete,
                                payload: {
                                    title: `Вы уверены, что хотите удалить контакт ${original.name}?`,
                                    popupAction: ActionEnum.DELETE,
                                    onAction: async () => {
                                        try {
                                            await deleteContactAsync.current({
                                                id: original.id,
                                            });
                                        } catch (e) {
                                            console.error(e);
                                        }
                                    },
                                },
                            });
                        }}
                    >
                        удалить
                    </Button>
                </Layout.Item>
            </Layout>
        ),
        [dispatch, popupDispatch, store?.id]
    );

    const columns = useMemo(
        () => [
            columnHelper.accessor('id', {
                header: 'ID',
                cell: props => props.getValue(),
            }),
            columnHelper.accessor('name', {
                header: 'ФИО',
                cell: props => props.getValue(),
            }),
            columnHelper.accessor('phone', {
                header: 'Телефон',
                cell: props => props.getValue(),
            }),
            columnHelper.accessor('email', {
                header: 'Email',
                cell: props => props.getValue(),
            }),
            columnHelper.accessor('created_at', {
                header: 'Дата создания',
                cell: props => formatDate(new Date(props.getValue())),
            }),
            columnHelper.accessor('updated_at', {
                header: 'Дата изменения',
                cell: props => formatDate(new Date(props.getValue())),
            }),
            columnHelper.display({
                id: 'settings',
                header: '',
                cell: SettingsCell,
            }),
        ],
        [SettingsCell]
    );

    const [{ sorting }, sortingPlugin] = useSorting<StoreContact>('store_contacts');

    const table = useTable(
        {
            data,
            columns,
            meta: {
                tableKey: `store_contacts`,
            },
            state: {
                sorting,
            },
        },
        [sortingPlugin]
    );

    return (
        <LoadWrapper isLoading={deleteContact.isLoading}>
            <Block css={{ width: '100%', borderTopLeftRadius: 0, borderTopRightRadius: 0 }}>
                <Block.Body>
                    <div css={{ width: '100%', display: 'flex', marginBottom: scale(2) }}>
                        <Button
                            Icon={AddIcon}
                            onClick={() =>
                                dispatch({
                                    type: ActionType.Add,
                                })
                            }
                            css={{ marginLeft: 'auto' }}
                        >
                            Добавить контактное лицо
                        </Button>
                    </div>
                    <Table instance={table} />
                    <ActionPopup popupState={popupState} popupDispatch={popupDispatch} />
                </Block.Body>
            </Block>
        </LoadWrapper>
    );
};
