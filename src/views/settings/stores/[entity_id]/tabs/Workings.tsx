import { CellContext, createColumnHelper } from '@tanstack/react-table';
import { useMemo, useRef, useCallback } from 'react';

import { ActionEnum } from '@components/ActionPopup';
import Block from '@components/Block';
import LoadWrapper from '@components/controls/LoadWrapper';
import Table, { useSorting, useTable } from '@components/Table';

import { ActionType, daysValues } from '@scripts/enums';
import { Button, Layout, scale } from '@scripts/gds';
import { formatDate } from '@scripts/helpers';
import { useActionPopup } from '@scripts/hooks/useActionPopup';

import EditIcon from '@icons/small/edit.svg';
import AddIcon from '@icons/small/plus.svg';
import DeleteIcon from '@icons/small/trash.svg';

import { Store, StoreWorking } from '@api/settings/types';
import { useDeleteStoreWorking } from '@api/settings';
import { EditWorkingPopupProps } from '../components/EditWorkingPopup';

const columnHelper = createColumnHelper<StoreWorking>();

const initialSort = { id: 'id', desc: true };

/**
 * Таб "Время работы"
 */
export const Workings = ({ store, dispatch }: { store?: Store; dispatch: EditWorkingPopupProps['dispatch'] }) => {
    const data = useMemo(() => store?.workings || [], [store?.workings]);

    const deleteStoreWorking = useDeleteStoreWorking();

    const deleteWorkingAsync = useRef(deleteStoreWorking.mutateAsync);
    deleteWorkingAsync.current = deleteStoreWorking.mutateAsync;

    const { popupState, popupDispatch, ActionPopup } = useActionPopup();

    const SettingsCell = useCallback(
        ({ row: { original } }: CellContext<StoreWorking, unknown>) => (
            <Layout type="flex">
                <Layout.Item>
                    <Button
                        hidden
                        Icon={EditIcon}
                        onClick={() => {
                            dispatch({
                                type: ActionType.Edit,
                                payload: {
                                    data: {
                                        id: original.id,
                                        active: original.active,
                                        day: original.day,
                                        working_end_time: original.working_end_time,
                                        working_start_time: original.working_start_time,
                                    },
                                    open: true,
                                    storeId: store?.id!,
                                },
                            });
                        }}
                    >
                        изменить
                    </Button>
                </Layout.Item>
                <Layout.Item>
                    <Button
                        hidden
                        Icon={DeleteIcon}
                        theme="dangerous"
                        onClick={() => {
                            popupDispatch({
                                type: ActionType.Delete,
                                payload: {
                                    title: `Вы уверены, что хотите удалить время работы #${original.id}?`,
                                    popupAction: ActionEnum.DELETE,
                                    onAction: async () => {
                                        try {
                                            await deleteWorkingAsync.current({
                                                id: original.id,
                                            });
                                        } catch (e) {
                                            console.error(e);
                                        }
                                    },
                                },
                            });
                        }}
                    >
                        удалить
                    </Button>
                </Layout.Item>
            </Layout>
        ),
        [dispatch, popupDispatch, store?.id]
    );

    const columns = useMemo(
        () => [
            columnHelper.accessor('id', {
                header: 'ID',
                cell: props => props.getValue(),
            }),
            columnHelper.accessor('active', {
                header: 'Активность',
                cell: props => (props.getValue() ? 'да' : 'нет'),
            }),
            columnHelper.accessor('day', {
                header: 'День недели',
                cell: props => (daysValues as Record<any, string>)[props.getValue()] || '-',
            }),
            columnHelper.accessor('created_at', {
                header: 'Дата создания',
                cell: props => formatDate(new Date(props.getValue())),
            }),
            columnHelper.accessor('updated_at', {
                header: 'Дата изменения',
                cell: props => formatDate(new Date(props.getValue())),
            }),
            columnHelper.display({
                id: 'settings',
                header: '',
                cell: SettingsCell,
            }),
        ],
        [SettingsCell]
    );

    const [{ invertedSorting }, sortingPlugin] = useSorting<StoreWorking>('store_workings', initialSort);

    const table = useTable(
        {
            data,
            columns,
            meta: {
                tableKey: `store_workings`,
            },
            state: {
                sorting: invertedSorting,
            },
        },
        [sortingPlugin]
    );

    return (
        <LoadWrapper isLoading={deleteStoreWorking.isLoading}>
            <Block css={{ width: '100%', borderTopLeftRadius: 0, borderTopRightRadius: 0 }}>
                <Block.Body>
                    <div css={{ width: '100%', display: 'flex', marginBottom: scale(2) }}>
                        <Button
                            Icon={AddIcon}
                            onClick={() =>
                                dispatch({
                                    type: ActionType.Add,
                                })
                            }
                            css={{ marginLeft: 'auto' }}
                        >
                            Добавить время работы
                        </Button>
                    </div>
                    <Table instance={table} />
                    <ActionPopup popupState={popupState} popupDispatch={popupDispatch} />
                </Block.Body>
            </Block>
        </LoadWrapper>
    );
};
