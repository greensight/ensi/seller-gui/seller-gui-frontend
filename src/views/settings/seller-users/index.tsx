import Link from 'next/link';
import { useRouter } from 'next/router';
import { useCallback, useMemo } from 'react';

import { useSellerUsersMeta, useSellerUsersSearch } from '@api/units';
import { SellerUser } from '@api/units/types';

import { useError } from '@context/modal';

import AutoFilters from '@components/AutoFilters';
import Block from '@components/Block';
import Table, { ExtendedRow, TooltipItem, TrProps, useSorting, useTable } from '@components/Table';
import { getSettingsColumn } from '@components/Table/columns';
import { TableEmpty, TableFooter, TableHeader } from '@components/Table/components';
import RowTooltipWrapper from '@components/Table/components/RowTooltipWrapper';

import { useAutoFilters, useAutoFutureColumns, useAutoTableData } from '@hooks/autoTable';

import { ITEMS_PER_PRODUCTS_PAGE } from '@scripts/constants';
import { Button, scale } from '@scripts/gds';
import { declOfNum, getPagination, getTotal, getTotalPages } from '@scripts/helpers';
import { useRedirectToNotEmptyPage, useTableList } from '@scripts/hooks';

import PlusIcon from '@icons/plus.svg';

const UsersList = () => {
    const { push, pathname } = useRouter();
    const { data: metaData, error: metaError } = useSellerUsersMeta();
    useError(metaError);
    const meta = metaData?.data;

    const [{ backendSorting }, sortingPlugin] = useSorting<SellerUser>(pathname, {
        id: meta?.default_sort || 'id',
        desc: true,
    });

    const autoFiltersData = useAutoFilters(meta);
    const {
        metaField,
        values,
        filtersActive,
        URLHelper,
        reset,
        searchRequestFilter,
        emptyInitialValues,
        searchRequestIncludes,
        clearInitialValue,
        codesToSortKeys,
    } = autoFiltersData;

    const columns = useAutoFutureColumns(metaData?.data);

    const { activePage, itemsPerPageCount, setItemsPerPageCount } = useTableList({
        defaultSort: meta?.default_sort,
        defaultPerPage: ITEMS_PER_PRODUCTS_PAGE,
        codesToSortKeys,
        pageKey: pathname,
    });

    const { data: sellerUsers, error } = useSellerUsersSearch(
        {
            include: searchRequestIncludes,
            sort: backendSorting,
            filter: searchRequestFilter,
            pagination: getPagination(activePage, itemsPerPageCount),
        },
        !!meta
    );
    useError(error);
    const goToDetailPage = useCallback(
        (originalRows: ExtendedRow['original'] | ExtendedRow['original'][] | undefined) => {
            if (!Array.isArray(originalRows))
                push({
                    pathname: `/settings/seller-users/${originalRows?.original.id}`,
                });
        },
        [pathname, push]
    );
    const tooltipItems: TooltipItem[] = useMemo(
        () => [
            {
                type: 'edit',
                text: 'Редактировать пользователя',
                action: goToDetailPage,
                isDisable: false,
            },
        ],
        [goToDetailPage]
    );
    const total = getTotal(sellerUsers);
    const totalPages = getTotalPages(sellerUsers, itemsPerPageCount);
    const rows = useAutoTableData<SellerUser>(sellerUsers?.data, metaField);
    const table = useTable(
        {
            data: rows,
            columns: [
                ...columns,
                getSettingsColumn({ visibleColumns: meta?.default_list, tooltipContent: tooltipItems }),
            ],
            meta: { tableKey: pathname },
        },
        [sortingPlugin]
    );
    const getTooltipForRow = useCallback(() => tooltipItems, [tooltipItems]);
    useRedirectToNotEmptyPage({ activePage, itemsPerPageCount, total, pageKey: pathname });

    const renderRow = useCallback(
        ({ children, ...props }: TrProps<any>) => (
            <RowTooltipWrapper
                {...props}
                getTooltipForRow={getTooltipForRow}
                onDoubleClick={() => {
                    push(`/settings/seller-users/${props.row?.original.id}`);
                }}
            >
                {children}
            </RowTooltipWrapper>
        ),
        [getTooltipForRow, push]
    );
    return (
        <Block css={{ marginBottom: scale(3) }}>
            <Block.Body>
                <AutoFilters
                    initialValues={values}
                    emptyInitialValues={emptyInitialValues}
                    onSubmit={URLHelper}
                    filtersActive={filtersActive}
                    css={{ marginBottom: scale(2) }}
                    meta={meta}
                    onResetFilters={reset}
                    clearInitialValue={clearInitialValue}
                />
                <TableHeader css={{ justifyContent: 'space-between' }}>
                    <p>
                        Найдено{' '}
                        {`${total} ${declOfNum(total, ['пользователь', 'пользователя', 'пользователей'])} продавца`}
                    </p>
                    <div css={{ display: 'flex' }}>
                        <Link href="/settings/seller-users/create" passHref>
                            <Button Icon={PlusIcon} as="a">
                                Создать нового пользователя продавца
                            </Button>
                        </Link>
                    </div>
                </TableHeader>
                <Table instance={table} Tr={renderRow} />
                {rows.length === 0 ? (
                    <TableEmpty
                        onResetFilters={reset}
                        filtersActive={filtersActive}
                        titleWithFilters="Пользователи не найдены"
                        titleWithoutFilters="Нет пользователей"
                        addItems={() => push(`${pathname}/create`)}
                    />
                ) : (
                    <TableFooter
                        pages={totalPages}
                        itemsPerPageCount={itemsPerPageCount}
                        setItemsPerPageCount={setItemsPerPageCount}
                        pageKey={pathname}
                    />
                )}
            </Block.Body>
        </Block>
    );
};
export default UsersList;
