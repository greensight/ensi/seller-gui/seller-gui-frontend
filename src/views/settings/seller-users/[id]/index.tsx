import { useRouter } from 'next/router';
import { useMemo, useRef } from 'react';
import * as Yup from 'yup';

import { useCurrentUser } from '@api/auth';
import { useCreateSellerUser, useSellerUser, useUpdateSellerUser } from '@api/units';
import { SellerUserMutate } from '@api/units/types';

import { useError, useSuccess } from '@context/modal';

import Legend from '@controls/Legend';
import Mask from '@controls/Mask';
import Password from '@controls/Password';
import Switcher from '@controls/Switcher';
import Form from '@controls/future/Form';

import Block from '@components/Block';
import CopyButton from '@components/CopyButton';
import FormWrapper from '@components/FormWrapper';
import InfoList from '@components/InfoList';
import { getDefaultDates } from '@components/InfoList/helper';
import { InfoListItemCommonType } from '@components/InfoList/types';
import PageControls from '@components/PageControls';
import { PageTemplate } from '@components/PageTemplate';
import PageWrapper from '@components/PageWrapper';

import { CREATE_PARAM, ErrorMessages, ModalMessages } from '@scripts/constants';
import { RedirectMethods } from '@scripts/enums';
import { Layout, scale } from '@scripts/gds';
import { cleanPhoneValue, prepareTelValue } from '@scripts/helpers';
import { maskPhone } from '@scripts/mask';
import { regNameRu, regOneDigit, regOneLetter, regPhone } from '@scripts/regex';

function SellerUserDetail() {
    const {
        query: { id },
        push,
    } = useRouter();
    const isCreation = id === CREATE_PARAM;
    const userId = +`${id}`;
    const { data: currentUserData } = useCurrentUser();

    const isOwnerDetail = currentUserData?.data?.id === userId;
    const { data: apiSellerUser, isFetching: isLoading } = useSellerUser(userId, !Number.isNaN(userId));
    const sellerUser = apiSellerUser?.data;

    const title = isCreation
        ? 'Создание пользователя продавца'
        : `Редактирование пользователя продавца ${apiSellerUser?.data.seller_id}`;
    const redirectAfterSave = useRef(false);

    const initialValues = useMemo(
        () => ({
            seller_id: sellerUser?.seller_id || '',
            active: sellerUser?.active || false,
            login: sellerUser?.login || '',
            last_name: sellerUser?.last_name || '',
            first_name: sellerUser?.first_name || '',
            middle_name: sellerUser?.middle_name || '',
            email: sellerUser?.email || '',
            phone: sellerUser?.phone ? prepareTelValue(sellerUser.phone) : '',
            password: null,
        }),
        [sellerUser]
    );

    const updateSellerUser = useUpdateSellerUser();
    useError(updateSellerUser.error);
    useSuccess(updateSellerUser.isSuccess ? ModalMessages.SUCCESS_UPDATE : '');

    const createSellerUser = useCreateSellerUser();
    useError(createSellerUser.error);
    useSuccess(createSellerUser.isSuccess ? ModalMessages.SUCCESS_UPDATE : '');

    return (
        <PageWrapper title={title} isLoading={isLoading || updateSellerUser.isLoading}>
            <FormWrapper
                initialValues={initialValues}
                onSubmit={async values => {
                    if (isCreation) {
                        const data = {
                            ...values,
                            phone: cleanPhoneValue(values?.phone),
                            seller_id: Number(values.seller_id),
                        };
                        const response = await createSellerUser.mutateAsync(data);
                        if (redirectAfterSave.current) {
                            return {
                                method: RedirectMethods.push,
                                redirectPath: `/settings/seller-users`,
                            };
                        }
                        return {
                            method: RedirectMethods.replace,
                            redirectPath: `/settings/seller-users/${response.data.id}`,
                        };
                    }
                    const clearValues = {
                        ...values,
                        ...(values?.phone && { phone: cleanPhoneValue(values?.phone) }),
                        seller_id: Number(values.seller_id),
                    };
                    const changedValues = Object.keys(clearValues).reduce((acc, cur) => {
                        const key = cur as keyof typeof values;
                        if (clearValues[key] !== initialValues[key]) return { ...acc, [key]: values[key] };
                        return acc;
                    }, {} as SellerUserMutate);

                    const enrichedData: SellerUserMutate = {
                        ...changedValues,
                        ...(changedValues?.seller_id && { seller_id: Number(values.seller_id) }),
                        ...(changedValues?.phone && { phone: cleanPhoneValue(changedValues?.phone) }),
                    };

                    if (Object.keys(changedValues)?.length) {
                        await updateSellerUser.mutateAsync({ id: userId, ...enrichedData });
                    }
                    if (redirectAfterSave.current) {
                        return { method: RedirectMethods.push, redirectPath: `/settings/seller-users` };
                    }
                    return {
                        method: RedirectMethods.replace,
                        redirectPath: `/settings/seller-users/${userId}`,
                    };
                }}
                validationSchema={Yup.object().shape({
                    active: Yup.boolean(),
                    login: Yup.string().required(ErrorMessages.REQUIRED),
                    first_name: Yup.string()
                        .matches(regNameRu, 'Используйте кириллицу для заполнения')
                        .required(ErrorMessages.REQUIRED),
                    last_name: Yup.string()
                        .matches(regNameRu, 'Используйте кириллицу для заполнения')
                        .required(ErrorMessages.REQUIRED),
                    middle_name: Yup.string()
                        .matches(regNameRu, 'Используйте кириллицу для заполнения')
                        .required(ErrorMessages.REQUIRED),
                    email: Yup.string().email(ErrorMessages.EMAIL).required(ErrorMessages.REQUIRED),
                    phone: Yup.string().matches(regPhone, 'Проверьте телефонный формат').required('Обязательное поле'),
                    password: Yup.string()
                        .matches(regOneLetter, 'Пароль должен содержать хотя бы 1 латинскую букву')
                        .matches(regOneDigit, 'Пароль должен содержать хотя бы 1 цифру')
                        .min(8, 'Пароль должен быть не менее 8 символов')
                        .when('$isCreation', (_, schema) =>
                            isCreation ? schema.required(ErrorMessages.REQUIRED) : schema.nullable()
                        ),
                })}
                enableReinitialize
            >
                <PageTemplate
                    backlink={{ href: `/settings/seller-users`, text: 'Назад' }}
                    h1={title}
                    customChildren
                    controls={
                        <PageControls gap={scale(1)}>
                            <PageControls.Close
                                onClick={() => {
                                    push(`/settings/seller-users`);
                                }}
                            />
                            <PageControls.Apply
                                onClick={() => {
                                    redirectAfterSave.current = false;
                                }}
                            />
                            <PageControls.Save
                                onClick={() => {
                                    redirectAfterSave.current = true;
                                }}
                            />
                        </PageControls>
                    }
                    aside={
                        <InfoList>
                            <Form.Field
                                name="active"
                                label="Активность"
                                disabled={isOwnerDetail}
                                css={{ marginBottom: scale(2) }}
                            >
                                <Switcher>Активность</Switcher>
                            </Form.Field>
                            <InfoList.Item name="ID:" value={!isCreation ? <CopyButton>{`${id}`}</CopyButton> : '-'} />
                            {(getDefaultDates(sellerUser || {}) as InfoListItemCommonType[]).map(item => (
                                <InfoList.Item key={item.name} {...item} />
                            ))}
                        </InfoList>
                    }
                >
                    <Block css={{ width: '100%' }}>
                        <Block.Body>
                            <Layout cols={2}>
                                <Layout.Item col={{ xxxl: 1, xxs: 2 }}>
                                    <Form.Field name="login" label="Логин" autoComplete="off" />
                                </Layout.Item>
                                <Layout.Item col={{ xxxl: 1, xxs: 2 }}>
                                    <Form.Field name="last_name" label="Фамилия" />
                                </Layout.Item>
                                <Layout.Item col={{ xxxl: 1, xxs: 2 }}>
                                    <Form.Field name="first_name" label="Имя" />
                                </Layout.Item>
                                <Layout.Item col={{ xxxl: 1, xxs: 2 }}>
                                    <Form.Field name="middle_name" label="Отчество" />
                                </Layout.Item>
                                <Layout.Item col={{ xxxl: 1, xxs: 2 }}>
                                    <Form.Field name="email" label="Email" autoComplete="off" />
                                </Layout.Item>
                                <Layout.Item col={{ xxxl: 1, xxs: 2 }}>
                                    <Form.Field name="phone" label="Телефон">
                                        <Mask mask={maskPhone} />
                                    </Form.Field>
                                </Layout.Item>
                                <Layout.Item col={{ xxxl: 2, xs: 2 }}>
                                    <Form.Field name="password">
                                        <Legend
                                            hint="Пароль должен быть не менее 8 символов и содержать минимум 1 латинский символ"
                                            label="Пароль"
                                        />
                                        <Password autoComplete="new-password" />
                                    </Form.Field>
                                </Layout.Item>
                            </Layout>
                        </Block.Body>
                    </Block>
                </PageTemplate>
            </FormWrapper>
        </PageWrapper>
    );
}

export default SellerUserDetail;
