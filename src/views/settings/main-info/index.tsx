import { useMemo, useRef } from 'react';
import * as Yup from 'yup';

import { usePatchSeller, useSellerDetail, useSellerStatuses } from '@api/settings/main-info';

import { useError, useSuccess } from '@context/modal';

import Mask from '@controls/Mask';
import Tabs from '@controls/Tabs';
import Select from '@controls/future/Select';
import Textarea from '@controls/future/TextArea';

import Block from '@components/Block';
import CopyButton from '@components/CopyButton';
import FormWrapper from '@components/FormWrapper';
import InfoList from '@components/InfoList';
import { getDefaultDates } from '@components/InfoList/helper';
import { InfoListItemCommonType } from '@components/InfoList/types';
import PageControls from '@components/PageControls';
import { PageTemplate } from '@components/PageTemplate';
import PageWrapper from '@components/PageWrapper';
import Form from '@components/controls/future/Form';

import { ErrorMessages, ModalMessages } from '@scripts/constants';
import { RedirectMethods } from '@scripts/enums';
import { Layout, scale } from '@scripts/gds';
import { toFutureSelectItems } from '@scripts/helpers';
import { useFutureTabs } from '@scripts/hooks/useFutureTabs';
import { maskCorpInn, maskNineDigits, maskTwentyDigits } from '@scripts/mask';
import validateInn from '@scripts/validateInn';

function SellerDetail() {
    const { data: apiSeller, isFetching: isLoading } = useSellerDetail();
    const seller = apiSeller?.data;
    const sellerId = seller?.id;
    const title = `Основные данные продавца ${seller?.legal_name}`;

    const { getTabsProps } = useFutureTabs();
    const redirectAfterSave = useRef(false);

    const initialValues = useMemo(
        () => ({
            legal_name: seller?.legal_name || '',
            inn: seller?.inn || '',
            kpp: seller?.kpp || '',
            payment_account: seller?.payment_account || '',
            correspondent_account: seller?.correspondent_account || '',
            bank_bik: seller?.bank_bik || '',
            bank: seller?.bank || '',
            status: seller?.status || '',
            manager_id: seller?.manager_id || '',
            legal_address: seller?.legal_address?.address_string || '',
            bank_address: seller?.bank_address?.address_string || '',
            fact_address: seller?.fact_address?.address_string || '',
            site: seller?.site || '',
            info: seller?.info || '',
        }),
        [seller]
    );

    const patchSeller = usePatchSeller();
    useError(patchSeller.error);
    useSuccess(patchSeller.isSuccess ? ModalMessages.SUCCESS_UPDATE : '');

    const { data: apiStatuses } = useSellerStatuses();
    const statusOptions = useMemo(() => toFutureSelectItems(apiStatuses?.data), [apiStatuses?.data]);

    return (
        <PageWrapper title={title} isLoading={isLoading || patchSeller.isLoading}>
            <FormWrapper
                initialValues={initialValues}
                onSubmit={async vals => {
                    const data = {
                        ...vals,
                        payment_account: `${vals.payment_account}`,
                        correspondent_account: `${vals.correspondent_account}`,
                        manager_id: +vals.manager_id,
                        legal_address: { address_string: vals.legal_address! },
                        fact_address: { address_string: vals.fact_address! },
                        bank_address: { address_string: vals.bank_address! },
                        status: +vals.status,
                    };

                    if (sellerId) {
                        await patchSeller.mutateAsync({
                            ...data,
                        });
                    }

                    return {
                        method: RedirectMethods.replace,
                        redirectPath: `/settings/main-info`,
                    };
                }}
                validationSchema={Yup.object().shape({
                    legal_name: Yup.string().required(ErrorMessages.REQUIRED),
                    legal_address: Yup.string().required(ErrorMessages.REQUIRED),
                    status: Yup.number()
                        .transform(e => (Number.isNaN(e) ? undefined : e))
                        .required(ErrorMessages.REQUIRED),
                    inn: Yup.string()
                        .required(ErrorMessages.REQUIRED)
                        .test('inn', 'Проверьте формат ИНН', value => validateInn(value))
                        .test('length', 'Введите ИНН юридического лица', value => value?.length !== 12),
                    bank_bik: Yup.string()
                        .required(ErrorMessages.REQUIRED)
                        .test(
                            'bank_bik',
                            'БИК всегда состоит из 9 цифр',
                            value => !`${value}`.length || `${value}`.length === 9
                        ),
                    bank: Yup.string().required(ErrorMessages.REQUIRED),
                    bank_address: Yup.string().required(ErrorMessages.REQUIRED),
                    fact_address: Yup.string().required(ErrorMessages.REQUIRED),
                    site: Yup.string().required(ErrorMessages.REQUIRED),
                    info: Yup.string().required(ErrorMessages.REQUIRED),
                    kpp: Yup.string()
                        .required(ErrorMessages.REQUIRED)
                        .test(
                            'kpp',
                            'КПП всегда состоит из 9 цифр',
                            value => !`${value}`.length || `${value}`.length === 9
                        ),
                    correspondent_account: Yup.string()
                        .required(ErrorMessages.REQUIRED)
                        .test('length', 'Введите 20 цифр', value => value?.length === 20),
                    payment_account: Yup.string()
                        .required(ErrorMessages.REQUIRED)
                        .test('length', 'Введите 20 цифр', value => value?.length === 20),
                })}
                enableReinitialize
            >
                <PageTemplate
                    h1={title}
                    customChildren
                    controls={
                        <PageControls gap={scale(1)}>
                            <PageControls.Save
                                onClick={() => {
                                    redirectAfterSave.current = false;
                                }}
                            />
                        </PageControls>
                    }
                    aside={
                        <InfoList>
                            <InfoList.Item name="ID:" value={<CopyButton>{`${sellerId}`}</CopyButton>} />
                            {(getDefaultDates(seller || {}) as InfoListItemCommonType[]).map(item => (
                                <InfoList.Item key={item.name} {...item} />
                            ))}
                        </InfoList>
                    }
                >
                    <Layout cols={1} css={{ flexGrow: 2 }}>
                        <Tabs {...getTabsProps()}>
                            <Tabs.Tab title="Основные данные" id="0">
                                <Block css={{ width: '100%' }}>
                                    <Block.Body>
                                        <Layout cols={2}>
                                            <Layout.Item>
                                                <Form.Field name="legal_name" label="Юридическое наименование" />
                                            </Layout.Item>
                                            <Layout.Item>
                                                <Form.Field name="legal_address" label="Юридический адрес" />
                                            </Layout.Item>
                                            <Layout.Item>
                                                <Form.Field name="inn" label="ИНН">
                                                    <Mask mask={maskCorpInn} />
                                                </Form.Field>
                                            </Layout.Item>
                                            <Layout.Item>
                                                <Form.Field name="kpp" label="КПП">
                                                    <Mask mask={maskNineDigits} />
                                                </Form.Field>
                                            </Layout.Item>
                                            <Layout.Item>
                                                <Form.Field name="payment_account" label="Расчетный счет">
                                                    <Mask mask={maskTwentyDigits} />
                                                </Form.Field>
                                            </Layout.Item>
                                            <Layout.Item>
                                                <Form.Field name="correspondent_account" label="Корреспондентский счет">
                                                    <Mask mask={maskTwentyDigits} />
                                                </Form.Field>
                                            </Layout.Item>
                                            <Layout.Item>
                                                <Form.Field name="bank_bik" label="БИК">
                                                    <Mask mask={maskNineDigits} />
                                                </Form.Field>
                                            </Layout.Item>
                                            <Layout.Item>
                                                <Form.Field name="bank" label="Юридическое наименование банка" />
                                            </Layout.Item>
                                            <Layout.Item>
                                                <Form.Field name="bank_address" label="Адрес банка" />
                                            </Layout.Item>
                                            <Layout.Item>
                                                <Form.Field name="status" label="Статус" disabled>
                                                    <Select options={statusOptions} hideClearButton />
                                                </Form.Field>
                                            </Layout.Item>
                                            <Layout.Item>
                                                <Form.Field name="fact_address" label="Фактический адрес" />
                                            </Layout.Item>
                                            <Layout.Item col={2}>
                                                <Form.Field name="manager_id" label="ID менеджера" disabled />
                                            </Layout.Item>
                                            <Layout.Item>
                                                <Form.Field name="site" label="Сайт продавца" />
                                            </Layout.Item>
                                            <Layout.Item col={2}>
                                                <Form.Field name="info" label="Информация о продавце">
                                                    <Textarea minRows={5} />
                                                </Form.Field>
                                            </Layout.Item>
                                        </Layout>
                                    </Block.Body>
                                </Block>
                            </Tabs.Tab>
                        </Tabs>
                    </Layout>
                </PageTemplate>
            </FormWrapper>
        </PageWrapper>
    );
}

export default SellerDetail;
