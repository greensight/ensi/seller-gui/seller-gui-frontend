import Link from 'next/link';

import { typography } from '@scripts/gds';
import { useLinkCSS } from '@scripts/hooks';

export default function NotFoundPage() {
    const linkStyle = useLinkCSS('blue');

    return (
        <div
            css={{
                height: '100%',
                width: '100%',
                display: 'flex',
                flexDirection: 'column',
                alignItems: 'center',
                justifyContent: 'center',
            }}
        >
            <h1 css={typography('h1')}>404 | Page not found</h1>
            <Link href="/" css={linkStyle}>
                Вернуться на главную
            </Link>
        </div>
    );
}
