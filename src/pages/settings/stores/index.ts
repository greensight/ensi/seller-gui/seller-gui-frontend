import getAuthData, { AuthProps } from '@scripts/getAuthData';

export { default } from '@views/settings/stores';

export async function getServerSideProps(data: AuthProps) {
    const { ...totalData } = await getAuthData({ ...data });

    return {
        props: {
            ...totalData,
        },
    };
}
