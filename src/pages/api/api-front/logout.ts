import { serialize } from 'cookie';
import { NextApiRequest, NextApiResponse } from 'next';

import { HttpCode } from '@scripts/constants';

export default async (req: NextApiRequest, res: NextApiResponse) => {
    if (req.method !== 'GET') {
        res.status(HttpCode.NOT_ALLOWED).json({ message: 'Method not allowed' });
    }

    const defaultCookieOptions = {
        secure: true,
        httpOnly: true,
        path: '/',
    };

    const API_HOST = process.env.SELLER_GUI_FRONTEND_SERVICE_HOST || '';
    // eslint-disable-next-line no-nested-ternary
    const host = API_HOST ? (API_HOST.slice(-1) === '/' ? API_HOST.slice(0, API_HOST.length - 1) : API_HOST) : '';

    const { access_token } = req.cookies;

    await fetch(`${host}/api/v1/auth/logout`, {
        method: 'GET',
        headers: {
            'Content-Type': 'application/json',
            Authorization: `Bearer ${access_token}`,
        },
    });

    res.setHeader('Set-Cookie', [
        serialize('access_token', '', { maxAge: 0, ...defaultCookieOptions }),
        serialize('refresh_token', '', { maxAge: 0, ...defaultCookieOptions }),
        serialize('expires_at', '', { maxAge: 0, ...defaultCookieOptions }),
    ]);

    res.status(200).end(JSON.stringify({ data: null }));
};
