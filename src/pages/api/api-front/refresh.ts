import { serialize } from 'cookie';
import { NextApiRequest, NextApiResponse } from 'next';

import { HttpCode, MAX_AGE_MONTH, MILLISECONDS_IN_SECOND } from '@scripts/constants';

export default async (req: NextApiRequest, res: NextApiResponse) => {
    if (req.method !== 'POST') {
        res.status(HttpCode.NOT_ALLOWED).json({ message: 'Method not allowed' });
    }

    const { refresh_token } = req.cookies;

    try {
        if (refresh_token) {
            const API_HOST = process.env.SELLER_GUI_FRONTEND_SERVICE_HOST || '';
            // eslint-disable-next-line no-nested-ternary
            const host = API_HOST
                ? API_HOST.slice(-1) === '/'
                    ? API_HOST.slice(0, API_HOST.length - 1)
                    : API_HOST
                : '';

            const data = {
                refresh_token,
            };

            const defaultCookieOptions = {
                secure: true,
                httpOnly: true,
                path: '/',
            };

            const response = await fetch(`${host}/api/v1/auth/refresh`, {
                method: 'POST',
                headers: {
                    'Content-Type': 'application/json',
                },
                body: JSON.stringify(data),
            });
            const responseJSON = await response.json();

            if (responseJSON.errors) {
                res.setHeader('Set-Cookie', [
                    serialize('access_token', '', { maxAge: 0, ...defaultCookieOptions }),
                    serialize('refresh_token', '', { maxAge: 0, ...defaultCookieOptions }),
                    serialize('expires_at', '', { maxAge: 0, ...defaultCookieOptions }),
                ]);

                res.status(response.status).end(JSON.stringify(responseJSON));
            } else {
                res.setHeader('Set-Cookie', [
                    serialize('access_token', responseJSON?.data.access_token, {
                        maxAge: responseJSON?.data.expires_in,
                        ...defaultCookieOptions,
                    }),
                    serialize('refresh_token', responseJSON?.data.refresh_token, {
                        maxAge: MAX_AGE_MONTH,
                        ...defaultCookieOptions,
                    }),
                    serialize(
                        'expires_at',
                        String(Date.now() / MILLISECONDS_IN_SECOND + +(responseJSON?.data.expires_in || 0)),
                        {
                            maxAge: MAX_AGE_MONTH,
                            ...defaultCookieOptions,
                        }
                    ),
                ]);

                const result = {
                    accessToken: responseJSON.data.access_token,
                    hasRefreshToken: Boolean(responseJSON.data.refresh_token),
                    expiresAt: String(Date.now() / MILLISECONDS_IN_SECOND + +responseJSON.data.expires_in),
                };

                res.status(200).end(JSON.stringify({ data: result }));
            }
        } else {
            res.status(400).json({ message: `Incorrect data` });
        }
    } catch {
        res.status(500).json({ message: `Internal Server Error` });
    }
};
