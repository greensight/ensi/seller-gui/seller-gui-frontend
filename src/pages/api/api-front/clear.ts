import { serialize } from 'cookie';
import { NextApiRequest, NextApiResponse } from 'next';

import { HttpCode } from '@scripts/constants';

export default async (req: NextApiRequest, res: NextApiResponse) => {
    if (req.method !== 'POST') {
        res.status(HttpCode.NOT_ALLOWED).json({ message: 'Method not allowed' });
    }

    const defaultCookieOptions = {
        secure: true,
        httpOnly: true,
        path: '/',
    };

    res.setHeader('Set-Cookie', [
        serialize('access_token', '', { maxAge: 0, ...defaultCookieOptions }),
        serialize('refresh_token', '', { maxAge: 0, ...defaultCookieOptions }),
        serialize('expires_at', '', { maxAge: 0, ...defaultCookieOptions }),
    ]);
    res.status(200).end(JSON.stringify({ message: null }));
};
