# Seller-gui-frontend

## Резюме

Название: seller-gui-frontend

Назначение: Frontend часть интерфейса продавца

## Разработка сервиса

Инструкцию описывающую разворот, запуск и тестирование сервиса на локальной машине можно найти в отдельном документе
в [Gitlab Pages](https://ensi-platform.gitlab.io/installation/local/frontend)

## Среды

### Test

CI: ----

URL: -----

### Preprod

Отсутствует

### Prod

Отсутствует

## Контакты

Команда поддерживающая данный сервис: https://gitlab.com/groups/greensight/ensi/-/group_members
Email для связи: mail@greensight.ru

## Лицензия

[Открытая лицензия на право использования программы для ЭВМ Greensight Ecom Platform (GEP)](LICENSE.md).
