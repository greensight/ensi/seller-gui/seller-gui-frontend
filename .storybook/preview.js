import { ThemeProvider as AutokitsThemeProvider } from '@greensight/gds/autokits';
import { INITIAL_VIEWPORTS } from '@storybook/addon-viewport';
import { RouterContext } from 'next/dist/shared/lib/router-context';
import Image from 'next/image';
import { QueryClient, QueryClientProvider } from 'react-query';

import { ThemeProvider, scale, theme } from '@scripts/gds';

import { BodyScrollLockProvider } from '../src/context/bodyScrollLock';

// Image = props => <img {...props} />;

export const parameters = {
    actions: { argTypesRegex: '^on[A-Z].*' },
    controls: {
        matchers: {
            color: /(background|color)$/i,
            date: /Date$/,
        },
        hideNoControlsWarning: true,
    },
    viewport: {
        viewports: INITIAL_VIEWPORTS,
    },
    paddings: {
        values: [
            { name: 'None', value: '0' },
            { name: 'Small', value: '16px' },
            { name: 'Medium', value: '32px' },
            { name: 'Large', value: '64px' },
        ],
        default: 'Medium',
    },
    backgrounds: {
        grid: { cellSize: scale(1) },
        values: theme.colors && Object.entries(theme.colors).map(([name, value]) => ({ name, value })),
    },
    options: {
        storySort: {
            order: ['Intro', 'Autokits', 'Components'],
        },
    },
    nextRouter: {
        Provider: RouterContext.Provider,
    },
    parameters: {
        nextRouter: {
            path: '/products/[id]',
            asPath: '/products/catalog',
            query: {
                id: 'catalog',
            },
        },
    },
};

const queryClient = new QueryClient({
    defaultOptions: {
        queries: {
            staleTime: 10000,
            retry: 0,
            refetchOnWindowFocus: false,
        },
    },
});

export const decorators = [
    Story => {
        return (
            <AutokitsThemeProvider theme={theme}>
                <ThemeProvider theme={theme}>
                    <BodyScrollLockProvider>
                        <QueryClientProvider client={queryClient}>
                            <Story />
                        </QueryClientProvider>
                    </BodyScrollLockProvider>
                </ThemeProvider>
            </AutokitsThemeProvider>
        );
    },
];
